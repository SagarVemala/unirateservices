package com.unirateservices.universal_rate.service;

import com.unirateservices.universal_rate.ImPolicyInfo;
import com.unirateservices.universal_rate.models.procedure.CopyQuoteProcResponse;
import com.unirateservices.universalraterutilsservice.UniversalRaterUtilsService;
import com.wavemaker.runtime.data.dao.procedure.WMProcedureExecutor;
import com.wavemaker.runtime.security.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

public class PostProcessUNIVERSAL_RATEProcedureExecutorServiceImpl extends UNIVERSAL_RATEProcedureExecutorServiceImpl {

    private  static final Logger LOGGER = LoggerFactory.getLogger(PostProcessUNIVERSAL_RATEProcedureExecutorServiceImpl.class);

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UniversalRaterUtilsService universalRaterUtilsService;

    @Autowired
    private ImPolicyInfoService imPolicyInfoService;

    @Autowired
    @Qualifier("UNIVERSAL_RATEWMProcedureExecutor")
    private WMProcedureExecutor procedureExecutor;

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public CopyQuoteProcResponse executeCopyQuoteProc(Integer policyId, Integer newPolicyId) {
        Map<String, Object> params = new HashMap<>(2);
        CopyQuoteProcResponse copyQuoteProcResponse;

        params.put("policy_id", policyId);
        params.put("new_policy_id", newPolicyId);

        copyQuoteProcResponse=procedureExecutor.executeNamedProcedure("CopyQuoteProc", params, CopyQuoteProcResponse.class);
        //Quering to DB for getting Data to log
        ImPolicyInfo imPolicyInfo=getQuoteDetails(copyQuoteProcResponse.getNewPolicyId());
        LOGGER.info("REQUOTE|src/main/java/com/unirateservices/universal_rate/service/PostProcessUNIVERSAL_RATEProcedureExecutorServiceImpl.java:36|{}|{}|{}|{}|ReQuote Event occurred",imPolicyInfo.getInsertedBy(),imPolicyInfo.getPolicyId(),imPolicyInfo.getEffective(),imPolicyInfo.getRiskState() );

        return copyQuoteProcResponse;
    }

    // Method to fetch data for logging
    private ImPolicyInfo getQuoteDetails(Integer newPolicyId){
        ImPolicyInfo imPolicyInfo=new ImPolicyInfo();
        try{
            imPolicyInfo=imPolicyInfoService.getById(newPolicyId);
        }
        catch (Exception e){
            return   imPolicyInfo;
        }
       return imPolicyInfo;
    }

}
