package com.unirateservices.universal_rate.service;

import com.unirateservices.universal_rate.models.query.UpdateDropdownListLockRequest;
import com.unirateservices.universal_rate.models.query.UpdateQuestionMasterLockRequest;
import com.unirateservices.universal_rate.models.query.UpdateStatesForCarrierRequest;
import com.unirateservices.universalraterutilsservice.UniversalRaterUtilsService;
import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.security.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

public class PostProcessUNIVERSAL_RATEQueryExecutorService extends UNIVERSAL_RATEQueryExecutorServiceImpl{
    private static final Logger LOGGER = LoggerFactory.getLogger(PostProcessUNIVERSAL_RATEQueryExecutorService.class);

    @Autowired
    @Qualifier("UNIVERSAL_RATEWMQueryExecutor")
    private WMQueryExecutor queryExecutor;

    @Autowired
    private SecurityService securityService;


    @Autowired
    private UniversalRaterUtilsService universalRaterUtilsService;

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Integer executeUpdateStatesForCarrier(UpdateStatesForCarrierRequest updateStatesForCarrierRequest) {
        Map<String, Object> params = new HashMap<>(2);
        Integer response=0;

        params.put("states", updateStatesForCarrierRequest.getStates());
        params.put("carrier", updateStatesForCarrierRequest.getCarrier());

        response= queryExecutor.executeNamedQueryForUpdate("UpdateStatesForCarrier", params);
        LOGGER.info("CARRIER_UPDATE|src/main/java/com/unirateservices/universal_rate/service/PostProcessUNIVERSAL_RATEQueryExecutorService.java:34|{}|{}|{}|Carrier Updated",securityService.getUserId(),updateStatesForCarrierRequest.getCarrier(),updateStatesForCarrierRequest.getStates() );
        return response;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Integer executeUpdateDropdownListLock(UpdateDropdownListLockRequest updateDropdownListLockRequest) {
        Map<String, Object> params = new HashMap<>(2);
        Integer response=0;

        params.put("defaultLock", updateDropdownListLockRequest.getDefaultLock());
        params.put("dropDownListId", updateDropdownListLockRequest.getDropDownListId());

        response= queryExecutor.executeNamedQueryForUpdate("UpdateDropdownListLock", params);
        LOGGER.info("GENERAL_DEFAULT_UPDATE|src/main/java/com/unirateservices/universal_rate/service/PostProcessUNIVERSAL_RATEQueryExecutorService.java:48|{}|{}|{}|General Default updated",securityService.getUserId(),updateDropdownListLockRequest.getDropDownListId(),updateDropdownListLockRequest.getDefaultLock() );
        return response;
    }
    


}
