package com.unirateservices.universal_rate.service;

import com.unirateservices.universal_rate.ImPolicyInfo;
import com.unirateservices.universalraterutilsservice.UniversalRaterUtilsService;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.security.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

public class PostProcessImPolicyInfoServiceImpl extends ImPolicyInfoServiceImpl{

    private static final Logger LOGGER = LoggerFactory.getLogger(PostProcessImPolicyInfoServiceImpl.class);

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UniversalRaterUtilsService universalRaterUtilsService;

    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImPolicyInfoDao")
    private WMGenericDao<ImPolicyInfo, Integer> wmGenericDao;


    public void setWMGenericDao(WMGenericDao<ImPolicyInfo, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImPolicyInfo create(ImPolicyInfo imPolicyInfo) {
        LOGGER.debug("Creating a new ImPolicyInfo with information: {} || ", imPolicyInfo);

        try{
            ImPolicyInfo imPolicyInfoCreated = this.wmGenericDao.create(imPolicyInfo);

            // reloading object from database to get database defined & server defined values.
            imPolicyInfoCreated =this.wmGenericDao.refresh(imPolicyInfoCreated);
            LOGGER.info("QUOTE_CREATION|src/main/java/com/unirateservices/universal_rate/service/PostProcessImPolicyInfoServiceImpl.java:35|{}|{}|{}|{}|New Quote created",imPolicyInfoCreated.getInsertedBy(),imPolicyInfoCreated.getPolicyId(),imPolicyInfo.getEffective(),imPolicyInfo.getRiskState() );
            return imPolicyInfoCreated;
        }
        catch (Exception e){
            LOGGER.error("QUOTE_CREATION|src/main/java/com/unirateservices/universal_rate/service/PostProcessImPolicyInfoServiceImpl.java:35|"+securityService.getUserId().toString() +" | ");
            e.printStackTrace();
            return  null;
        }

    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImPolicyInfo update(ImPolicyInfo imPolicyInfo) {
        LOGGER.debug("Updating ImPolicyInfo with information: {}", imPolicyInfo);

        try{
            this.wmGenericDao.update(imPolicyInfo);
            this.wmGenericDao.refresh(imPolicyInfo);
            LOGGER.info("REQUOTE|src/main/java/com/unirateservices/universal_rate/service/PostProcessImPolicyInfoServiceImpl.java:47|{}|{}|{}|{}|ReQuote Event occurred",imPolicyInfo.getInsertedBy(),imPolicyInfo.getPolicyId(),imPolicyInfo.getEffective(),imPolicyInfo.getRiskState() );

            return imPolicyInfo;
        }
        catch (Exception e){
            LOGGER.info("REQUOTE|src/main/java/com/unirateservices/universal_rate/service/PostProcessImPolicyInfoServiceImpl.java:47|"+securityService.getUserId().toString() +"|");
            e.printStackTrace();
            return  null;
        }

    }
}
