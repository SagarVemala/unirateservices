/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.carrier;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

//import com.unirateservices.carrier.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class Carrier {
    private String quoteExecutionID;

    private String id;

    private String comprater;

    private String applicantID;

    private String company;

    private String name;

    private String policyNumber;

    private String insuraMatchID;
    
    private String totalPremium;
    
    public String getTotalPremium ()
    {
        return totalPremium;
    }

    public void setTotalPremium (String totalPremium)
    {
        this.totalPremium = totalPremium;
    }

    public String getQuoteExecutionID ()
    {
        return quoteExecutionID;
    }

    public void setQuoteExecutionID (String quoteExecutionID)
    {
        this.quoteExecutionID = quoteExecutionID;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getComprater ()
    {
        return comprater;
    }

    public void setComprater (String comprater)
    {
        this.comprater = comprater;
    }

    public String getApplicantID ()
    {
        return applicantID;
    }

    public void setApplicantID (String applicantID)
    {
        this.applicantID = applicantID;
    }

    public String getCompany ()
    {
        return company;
    }

    public void setCompany (String company)
    {
        this.company = company;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getPolicyNumber ()
    {
        return policyNumber;
    }

    public void setPolicyNumber (String policyNumber)
    {
        this.policyNumber = policyNumber;
    }

    public String getInsuraMatchID ()
    {
        return insuraMatchID;
    }

    public void setInsuraMatchID (String insuraMatchID)
    {
        this.insuraMatchID = insuraMatchID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [quoteExecutionID = "+quoteExecutionID+", id = "+id+", comprater = "+comprater+", applicantID = "+applicantID+", company = "+company+", name = "+name+", policyNumber = "+policyNumber+", insuraMatchID = "+insuraMatchID+"]";
    }
}