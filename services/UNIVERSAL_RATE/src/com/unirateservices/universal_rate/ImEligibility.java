/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ImEligibility generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`IM_ELIGIBILITY`")
public class ImEligibility implements Serializable {

    private Long eligibilityId;
    private Integer policyId;
    private String questionAnswer1;
    private String questionAnswer2;
    private String questionAnswer3;
    private String questionAnswer4;
    private String questionAnswer5;
    private String questionAnswer6;
    private String questionAnswer7;
    private String questionAnswer8;
    private String questionAnswer9;
    private String questionAnswer10;
    private LocalDateTime insertTimestamp;
    private LocalDateTime updateTimestamp;
    private String updatedBy;
    private String insertedBy;
    private Integer versionNum;
    private String questionAnswer11;
    private String questionAnswer12;
    private String questionAnswer13;
    private String questionAnswer14;
    private String questionAnswer15;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ELIGIBILITY_ID`", nullable = false, scale = 0, precision = 19)
    public Long getEligibilityId() {
        return this.eligibilityId;
    }

    public void setEligibilityId(Long eligibilityId) {
        this.eligibilityId = eligibilityId;
    }

    @Column(name = "`POLICY_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getPolicyId() {
        return this.policyId;
    }

    public void setPolicyId(Integer policyId) {
        this.policyId = policyId;
    }

    @Column(name = "`QUESTION_ANSWER_1`", nullable = true, length = 50)
    public String getQuestionAnswer1() {
        return this.questionAnswer1;
    }

    public void setQuestionAnswer1(String questionAnswer1) {
        this.questionAnswer1 = questionAnswer1;
    }

    @Column(name = "`QUESTION_ANSWER_2`", nullable = true, length = 50)
    public String getQuestionAnswer2() {
        return this.questionAnswer2;
    }

    public void setQuestionAnswer2(String questionAnswer2) {
        this.questionAnswer2 = questionAnswer2;
    }

    @Column(name = "`QUESTION_ANSWER_3`", nullable = true, length = 50)
    public String getQuestionAnswer3() {
        return this.questionAnswer3;
    }

    public void setQuestionAnswer3(String questionAnswer3) {
        this.questionAnswer3 = questionAnswer3;
    }

    @Column(name = "`QUESTION_ANSWER_4`", nullable = true, length = 50)
    public String getQuestionAnswer4() {
        return this.questionAnswer4;
    }

    public void setQuestionAnswer4(String questionAnswer4) {
        this.questionAnswer4 = questionAnswer4;
    }

    @Column(name = "`QUESTION_ANSWER_5`", nullable = true, length = 50)
    public String getQuestionAnswer5() {
        return this.questionAnswer5;
    }

    public void setQuestionAnswer5(String questionAnswer5) {
        this.questionAnswer5 = questionAnswer5;
    }

    @Column(name = "`QUESTION_ANSWER_6`", nullable = true, length = 50)
    public String getQuestionAnswer6() {
        return this.questionAnswer6;
    }

    public void setQuestionAnswer6(String questionAnswer6) {
        this.questionAnswer6 = questionAnswer6;
    }

    @Column(name = "`QUESTION_ANSWER_7`", nullable = true, length = 50)
    public String getQuestionAnswer7() {
        return this.questionAnswer7;
    }

    public void setQuestionAnswer7(String questionAnswer7) {
        this.questionAnswer7 = questionAnswer7;
    }

    @Column(name = "`QUESTION_ANSWER_8`", nullable = true, length = 50)
    public String getQuestionAnswer8() {
        return this.questionAnswer8;
    }

    public void setQuestionAnswer8(String questionAnswer8) {
        this.questionAnswer8 = questionAnswer8;
    }

    @Column(name = "`QUESTION_ANSWER_9`", nullable = true, length = 50)
    public String getQuestionAnswer9() {
        return this.questionAnswer9;
    }

    public void setQuestionAnswer9(String questionAnswer9) {
        this.questionAnswer9 = questionAnswer9;
    }

    @Column(name = "`QUESTION_ANSWER_10`", nullable = true, length = 50)
    public String getQuestionAnswer10() {
        return this.questionAnswer10;
    }

    public void setQuestionAnswer10(String questionAnswer10) {
        this.questionAnswer10 = questionAnswer10;
    }

    @Column(name = "`INSERT_TIMESTAMP`", nullable = true)
    public LocalDateTime getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(LocalDateTime insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    @Column(name = "`UPDATE_TIMESTAMP`", nullable = true)
    public LocalDateTime getUpdateTimestamp() {
        return this.updateTimestamp;
    }

    public void setUpdateTimestamp(LocalDateTime updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    @Column(name = "`UPDATED_BY`", nullable = true, length = 50)
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "`INSERTED_BY`", nullable = true, length = 50)
    public String getInsertedBy() {
        return this.insertedBy;
    }

    public void setInsertedBy(String insertedBy) {
        this.insertedBy = insertedBy;
    }

    @Column(name = "`VERSION_NUM`", nullable = true, scale = 0, precision = 10)
    public Integer getVersionNum() {
        return this.versionNum;
    }

    public void setVersionNum(Integer versionNum) {
        this.versionNum = versionNum;
    }

    @Column(name = "`QUESTION_ANSWER_11`", nullable = true, length = 50)
    public String getQuestionAnswer11() {
        return this.questionAnswer11;
    }

    public void setQuestionAnswer11(String questionAnswer11) {
        this.questionAnswer11 = questionAnswer11;
    }

    @Column(name = "`QUESTION_ANSWER_12`", nullable = true, length = 50)
    public String getQuestionAnswer12() {
        return this.questionAnswer12;
    }

    public void setQuestionAnswer12(String questionAnswer12) {
        this.questionAnswer12 = questionAnswer12;
    }

    @Column(name = "`QUESTION_ANSWER_13`", nullable = true, length = 50)
    public String getQuestionAnswer13() {
        return this.questionAnswer13;
    }

    public void setQuestionAnswer13(String questionAnswer13) {
        this.questionAnswer13 = questionAnswer13;
    }

    @Column(name = "`QUESTION_ANSWER_14`", nullable = true, length = 50)
    public String getQuestionAnswer14() {
        return this.questionAnswer14;
    }

    public void setQuestionAnswer14(String questionAnswer14) {
        this.questionAnswer14 = questionAnswer14;
    }

    @Column(name = "`QUESTION_ANSWER_15`", nullable = true, length = 50)
    public String getQuestionAnswer15() {
        return this.questionAnswer15;
    }

    public void setQuestionAnswer15(String questionAnswer15) {
        this.questionAnswer15 = questionAnswer15;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImEligibility)) return false;
        final ImEligibility imEligibility = (ImEligibility) o;
        return Objects.equals(getEligibilityId(), imEligibility.getEligibilityId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEligibilityId());
    }
}