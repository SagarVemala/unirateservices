/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateQuestionDefaultRequest implements Serializable {


    @JsonProperty("default_ind")
    @NotNull
    private String defaultInd;

    @JsonProperty("qmi")
    @NotNull
    private String qmi;

    @JsonProperty("qai")
    private String qai;

    public String getDefaultInd() {
        return this.defaultInd;
    }

    public void setDefaultInd(String defaultInd) {
        this.defaultInd = defaultInd;
    }

    public String getQmi() {
        return this.qmi;
    }

    public void setQmi(String qmi) {
        this.qmi = qmi;
    }

    public String getQai() {
        return this.qai;
    }

    public void setQai(String qai) {
        this.qai = qai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateQuestionDefaultRequest)) return false;
        final UpdateQuestionDefaultRequest updateQuestionDefaultRequest = (UpdateQuestionDefaultRequest) o;
        return Objects.equals(getDefaultInd(), updateQuestionDefaultRequest.getDefaultInd()) &&
                Objects.equals(getQmi(), updateQuestionDefaultRequest.getQmi()) &&
                Objects.equals(getQai(), updateQuestionDefaultRequest.getQai());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDefaultInd(),
                getQmi(),
                getQai());
    }
}