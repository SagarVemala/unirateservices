/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.util.Objects;

import com.wavemaker.runtime.data.annotations.ColumnAlias;

public class GetCoApplicantInfoByPolicyIdResponse implements Serializable {


    @ColumnAlias("applicant_id")
    private Integer applicantId;

    @ColumnAlias("policy_id")
    private Integer policyId;

    @ColumnAlias("applicant_type")
    private String applicantType;

    @ColumnAlias("first_name")
    private String firstName;

    @ColumnAlias("middle_name")
    private String middleName;

    @ColumnAlias("last_name")
    private String lastName;

    @ColumnAlias("suffix")
    private String suffix;

    @ColumnAlias("gender")
    private String gender;

    @ColumnAlias("DOB")
    private String dob;

    @ColumnAlias("education")
    private String education;

    @ColumnAlias("industry")
    private String industry;

    @ColumnAlias("occupation")
    private String occupation;

    @ColumnAlias("marital_status")
    private String maritalStatus;

    @ColumnAlias("SSN")
    private String ssn;

    @ColumnAlias("license_to_drive")
    private String licenseToDrive;

    @ColumnAlias("relation")
    private String relation;

    @ColumnAlias("address_id")
    private Integer addressId;

    @ColumnAlias("address_type")
    private String addressType;

    @ColumnAlias("address_code")
    private String addressCode;

    @ColumnAlias("street_name")
    private String streetName;

    @ColumnAlias("unit_number")
    private String unitNumber;

    @ColumnAlias("address_2")
    private String address2;

    @ColumnAlias("city")
    private String city;

    @ColumnAlias("state_code")
    private String stateCode;

    @ColumnAlias("county")
    private String county;

    @ColumnAlias("zip_code5")
    private String zipCode5;

    @ColumnAlias("zip_code4")
    private String zipCode4;

    @ColumnAlias("phone_type")
    private String phoneType;

    @ColumnAlias("phone_number_type")
    private String phoneNumberType;

    @ColumnAlias("phone_number")
    private String phoneNumber;

    @ColumnAlias("email")
    private String email;

    public Integer getApplicantId() {
        return this.applicantId;
    }

    public void setApplicantId(Integer applicantId) {
        this.applicantId = applicantId;
    }

    public Integer getPolicyId() {
        return this.policyId;
    }

    public void setPolicyId(Integer policyId) {
        this.policyId = policyId;
    }

    public String getApplicantType() {
        return this.applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEducation() {
        return this.education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getIndustry() {
        return this.industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getOccupation() {
        return this.occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getMaritalStatus() {
        return this.maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getSsn() {
        return this.ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getLicenseToDrive() {
        return this.licenseToDrive;
    }

    public void setLicenseToDrive(String licenseToDrive) {
        this.licenseToDrive = licenseToDrive;
    }

    public String getRelation() {
        return this.relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public Integer getAddressId() {
        return this.addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getAddressType() {
        return this.addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddressCode() {
        return this.addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public String getStreetName() {
        return this.streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getUnitNumber() {
        return this.unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCounty() {
        return this.county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getZipCode5() {
        return this.zipCode5;
    }

    public void setZipCode5(String zipCode5) {
        this.zipCode5 = zipCode5;
    }

    public String getZipCode4() {
        return this.zipCode4;
    }

    public void setZipCode4(String zipCode4) {
        this.zipCode4 = zipCode4;
    }

    public String getPhoneType() {
        return this.phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getPhoneNumberType() {
        return this.phoneNumberType;
    }

    public void setPhoneNumberType(String phoneNumberType) {
        this.phoneNumberType = phoneNumberType;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetCoApplicantInfoByPolicyIdResponse)) return false;
        final GetCoApplicantInfoByPolicyIdResponse getCoApplicantInfoByPolicyIdResponse = (GetCoApplicantInfoByPolicyIdResponse) o;
        return Objects.equals(getApplicantId(), getCoApplicantInfoByPolicyIdResponse.getApplicantId()) &&
                Objects.equals(getPolicyId(), getCoApplicantInfoByPolicyIdResponse.getPolicyId()) &&
                Objects.equals(getApplicantType(), getCoApplicantInfoByPolicyIdResponse.getApplicantType()) &&
                Objects.equals(getFirstName(), getCoApplicantInfoByPolicyIdResponse.getFirstName()) &&
                Objects.equals(getMiddleName(), getCoApplicantInfoByPolicyIdResponse.getMiddleName()) &&
                Objects.equals(getLastName(), getCoApplicantInfoByPolicyIdResponse.getLastName()) &&
                Objects.equals(getSuffix(), getCoApplicantInfoByPolicyIdResponse.getSuffix()) &&
                Objects.equals(getGender(), getCoApplicantInfoByPolicyIdResponse.getGender()) &&
                Objects.equals(getDob(), getCoApplicantInfoByPolicyIdResponse.getDob()) &&
                Objects.equals(getEducation(), getCoApplicantInfoByPolicyIdResponse.getEducation()) &&
                Objects.equals(getIndustry(), getCoApplicantInfoByPolicyIdResponse.getIndustry()) &&
                Objects.equals(getOccupation(), getCoApplicantInfoByPolicyIdResponse.getOccupation()) &&
                Objects.equals(getMaritalStatus(), getCoApplicantInfoByPolicyIdResponse.getMaritalStatus()) &&
                Objects.equals(getSsn(), getCoApplicantInfoByPolicyIdResponse.getSsn()) &&
                Objects.equals(getLicenseToDrive(), getCoApplicantInfoByPolicyIdResponse.getLicenseToDrive()) &&
                Objects.equals(getRelation(), getCoApplicantInfoByPolicyIdResponse.getRelation()) &&
                Objects.equals(getAddressId(), getCoApplicantInfoByPolicyIdResponse.getAddressId()) &&
                Objects.equals(getAddressType(), getCoApplicantInfoByPolicyIdResponse.getAddressType()) &&
                Objects.equals(getAddressCode(), getCoApplicantInfoByPolicyIdResponse.getAddressCode()) &&
                Objects.equals(getStreetName(), getCoApplicantInfoByPolicyIdResponse.getStreetName()) &&
                Objects.equals(getUnitNumber(), getCoApplicantInfoByPolicyIdResponse.getUnitNumber()) &&
                Objects.equals(getAddress2(), getCoApplicantInfoByPolicyIdResponse.getAddress2()) &&
                Objects.equals(getCity(), getCoApplicantInfoByPolicyIdResponse.getCity()) &&
                Objects.equals(getStateCode(), getCoApplicantInfoByPolicyIdResponse.getStateCode()) &&
                Objects.equals(getCounty(), getCoApplicantInfoByPolicyIdResponse.getCounty()) &&
                Objects.equals(getZipCode5(), getCoApplicantInfoByPolicyIdResponse.getZipCode5()) &&
                Objects.equals(getZipCode4(), getCoApplicantInfoByPolicyIdResponse.getZipCode4()) &&
                Objects.equals(getPhoneType(), getCoApplicantInfoByPolicyIdResponse.getPhoneType()) &&
                Objects.equals(getPhoneNumberType(), getCoApplicantInfoByPolicyIdResponse.getPhoneNumberType()) &&
                Objects.equals(getPhoneNumber(), getCoApplicantInfoByPolicyIdResponse.getPhoneNumber()) &&
                Objects.equals(getEmail(), getCoApplicantInfoByPolicyIdResponse.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getApplicantId(),
                getPolicyId(),
                getApplicantType(),
                getFirstName(),
                getMiddleName(),
                getLastName(),
                getSuffix(),
                getGender(),
                getDob(),
                getEducation(),
                getIndustry(),
                getOccupation(),
                getMaritalStatus(),
                getSsn(),
                getLicenseToDrive(),
                getRelation(),
                getAddressId(),
                getAddressType(),
                getAddressCode(),
                getStreetName(),
                getUnitNumber(),
                getAddress2(),
                getCity(),
                getStateCode(),
                getCounty(),
                getZipCode5(),
                getZipCode4(),
                getPhoneType(),
                getPhoneNumberType(),
                getPhoneNumber(),
                getEmail());
    }
}