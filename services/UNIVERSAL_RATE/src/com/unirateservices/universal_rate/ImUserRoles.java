/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ImUserRoles generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`IM_USER_ROLES`")
public class ImUserRoles implements Serializable {

    private Long roleId;
    private String roleName;
    private String roleType;
    private String activeInd;
    private LocalDateTime insertTimestamp;
    private String insertedBy;
    private LocalDateTime updateTimestamp;
    private String updatedBy;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ROLE_ID`", nullable = false, scale = 0, precision = 19)
    public Long getRoleId() {
        return this.roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Column(name = "`ROLE_NAME`", nullable = true, length = 60)
    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Column(name = "`ROLE_TYPE`", nullable = true, length = 60)
    public String getRoleType() {
        return this.roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    @Column(name = "`ACTIVE_IND`", nullable = true, length = 3)
    public String getActiveInd() {
        return this.activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    @Column(name = "`INSERT_TIMESTAMP`", nullable = true)
    public LocalDateTime getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(LocalDateTime insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    @Column(name = "`INSERTED_BY`", nullable = true, length = 50)
    public String getInsertedBy() {
        return this.insertedBy;
    }

    public void setInsertedBy(String insertedBy) {
        this.insertedBy = insertedBy;
    }

    @Column(name = "`UPDATE_TIMESTAMP`", nullable = true)
    public LocalDateTime getUpdateTimestamp() {
        return this.updateTimestamp;
    }

    public void setUpdateTimestamp(LocalDateTime updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    @Column(name = "`UPDATED_BY`", nullable = true, length = 50)
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImUserRoles)) return false;
        final ImUserRoles imUserRoles = (ImUserRoles) o;
        return Objects.equals(getRoleId(), imUserRoles.getRoleId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRoleId());
    }
}