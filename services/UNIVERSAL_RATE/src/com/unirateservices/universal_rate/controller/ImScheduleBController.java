/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.commons.wrapper.StringWrapper;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.manager.ExportedFileManager;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.unirateservices.universal_rate.ImScheduleB;
import com.unirateservices.universal_rate.service.ImScheduleBService;


/**
 * Controller object for domain model class ImScheduleB.
 * @see ImScheduleB
 */
@RestController("UNIVERSAL_RATE.ImScheduleBController")
@Api(value = "ImScheduleBController", description = "Exposes APIs to work with ImScheduleB resource.")
@RequestMapping("/UNIVERSAL_RATE/ImScheduleB")
public class ImScheduleBController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImScheduleBController.class);

    @Autowired
	@Qualifier("UNIVERSAL_RATE.ImScheduleBService")
	private ImScheduleBService imScheduleBService;

	@Autowired
	private ExportedFileManager exportedFileManager;

	@ApiOperation(value = "Creates a new ImScheduleB instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ImScheduleB createImScheduleB(@RequestBody ImScheduleB imScheduleB) {
		LOGGER.debug("Create ImScheduleB with information: {}" , imScheduleB);

		imScheduleB = imScheduleBService.create(imScheduleB);
		LOGGER.debug("Created ImScheduleB with information: {}" , imScheduleB);

	    return imScheduleB;
	}

    @ApiOperation(value = "Returns the ImScheduleB instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ImScheduleB getImScheduleB(@PathVariable("id") Integer id) {
        LOGGER.debug("Getting ImScheduleB with id: {}" , id);

        ImScheduleB foundImScheduleB = imScheduleBService.getById(id);
        LOGGER.debug("ImScheduleB details with id: {}" , foundImScheduleB);

        return foundImScheduleB;
    }

    @ApiOperation(value = "Updates the ImScheduleB instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ImScheduleB editImScheduleB(@PathVariable("id") Integer id, @RequestBody ImScheduleB imScheduleB) {
        LOGGER.debug("Editing ImScheduleB with id: {}" , imScheduleB.getScheduleBId());

        imScheduleB.setScheduleBId(id);
        imScheduleB = imScheduleBService.update(imScheduleB);
        LOGGER.debug("ImScheduleB details with id: {}" , imScheduleB);

        return imScheduleB;
    }

    @ApiOperation(value = "Deletes the ImScheduleB instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteImScheduleB(@PathVariable("id") Integer id) {
        LOGGER.debug("Deleting ImScheduleB with id: {}" , id);

        ImScheduleB deletedImScheduleB = imScheduleBService.delete(id);

        return deletedImScheduleB != null;
    }

    /**
     * @deprecated Use {@link #findImScheduleBs(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of ImScheduleB instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ImScheduleB> searchImScheduleBsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering ImScheduleBs list by query filter:{}", (Object) queryFilters);
        return imScheduleBService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of ImScheduleB instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ImScheduleB> findImScheduleBs(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering ImScheduleBs list by filter:", query);
        return imScheduleBService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of ImScheduleB instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ImScheduleB> filterImScheduleBs(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering ImScheduleBs list by filter", query);
        return imScheduleBService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportImScheduleBs(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return imScheduleBService.export(exportType, query, pageable);
    }

    @ApiOperation(value = "Returns a URL to download a file for the data matching the optional query (q) request param and the required fields provided in the Export Options.") 
    @RequestMapping(value = "/export", method = {RequestMethod.POST}, consumes = "application/json")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public StringWrapper exportImScheduleBsAndGetURL(@RequestBody DataExportOptions exportOptions, Pageable pageable) {
        String exportedFileName = exportOptions.getFileName();
        if(exportedFileName == null || exportedFileName.isEmpty()) {
            exportedFileName = ImScheduleB.class.getSimpleName();
        }
        exportedFileName += exportOptions.getExportType().getExtension();
        String exportedUrl = exportedFileManager.registerAndGetURL(exportedFileName, outputStream -> imScheduleBService.export(exportOptions, pageable, outputStream));
        return new StringWrapper(exportedUrl);
    }

	@ApiOperation(value = "Returns the total count of ImScheduleB instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countImScheduleBs( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting ImScheduleBs");
		return imScheduleBService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getImScheduleBAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return imScheduleBService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service ImScheduleBService instance
	 */
	protected void setImScheduleBService(ImScheduleBService service) {
		this.imScheduleBService = service;
	}

}