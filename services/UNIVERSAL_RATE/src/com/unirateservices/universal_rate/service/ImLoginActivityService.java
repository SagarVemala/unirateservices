/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.unirateservices.universal_rate.ImLoginActivity;

/**
 * Service object for domain model class {@link ImLoginActivity}.
 */
public interface ImLoginActivityService {

    /**
     * Creates a new ImLoginActivity. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on ImLoginActivity if any.
     *
     * @param imLoginActivity Details of the ImLoginActivity to be created; value cannot be null.
     * @return The newly created ImLoginActivity.
     */
    ImLoginActivity create(@Valid ImLoginActivity imLoginActivity);


	/**
     * Returns ImLoginActivity by given id if exists.
     *
     * @param imloginactivityId The id of the ImLoginActivity to get; value cannot be null.
     * @return ImLoginActivity associated with the given imloginactivityId.
	 * @throws EntityNotFoundException If no ImLoginActivity is found.
     */
    ImLoginActivity getById(Long imloginactivityId);

    /**
     * Find and return the ImLoginActivity by given id if exists, returns null otherwise.
     *
     * @param imloginactivityId The id of the ImLoginActivity to get; value cannot be null.
     * @return ImLoginActivity associated with the given imloginactivityId.
     */
    ImLoginActivity findById(Long imloginactivityId);

	/**
     * Find and return the list of ImLoginActivities by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param imloginactivityIds The id's of the ImLoginActivity to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return ImLoginActivities associated with the given imloginactivityIds.
     */
    List<ImLoginActivity> findByMultipleIds(List<Long> imloginactivityIds, boolean orderedReturn);


    /**
     * Updates the details of an existing ImLoginActivity. It replaces all fields of the existing ImLoginActivity with the given imLoginActivity.
     *
     * This method overrides the input field values using Server side or database managed properties defined on ImLoginActivity if any.
     *
     * @param imLoginActivity The details of the ImLoginActivity to be updated; value cannot be null.
     * @return The updated ImLoginActivity.
     * @throws EntityNotFoundException if no ImLoginActivity is found with given input.
     */
    ImLoginActivity update(@Valid ImLoginActivity imLoginActivity);

    /**
     * Deletes an existing ImLoginActivity with the given id.
     *
     * @param imloginactivityId The id of the ImLoginActivity to be deleted; value cannot be null.
     * @return The deleted ImLoginActivity.
     * @throws EntityNotFoundException if no ImLoginActivity found with the given id.
     */
    ImLoginActivity delete(Long imloginactivityId);

    /**
     * Deletes an existing ImLoginActivity with the given object.
     *
     * @param imLoginActivity The instance of the ImLoginActivity to be deleted; value cannot be null.
     */
    void delete(ImLoginActivity imLoginActivity);

    /**
     * Find all ImLoginActivities matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching ImLoginActivities.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<ImLoginActivity> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all ImLoginActivities matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching ImLoginActivities.
     *
     * @see Pageable
     * @see Page
     */
    Page<ImLoginActivity> findAll(String query, Pageable pageable);

    /**
     * Exports all ImLoginActivities matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all ImLoginActivities matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the ImLoginActivities in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the ImLoginActivity.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}