/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.unirateservices.universal_rate.ImDefaultValue;

/**
 * Service object for domain model class {@link ImDefaultValue}.
 */
public interface ImDefaultValueService {

    /**
     * Creates a new ImDefaultValue. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on ImDefaultValue if any.
     *
     * @param imDefaultValue Details of the ImDefaultValue to be created; value cannot be null.
     * @return The newly created ImDefaultValue.
     */
    ImDefaultValue create(@Valid ImDefaultValue imDefaultValue);


	/**
     * Returns ImDefaultValue by given id if exists.
     *
     * @param imdefaultvalueId The id of the ImDefaultValue to get; value cannot be null.
     * @return ImDefaultValue associated with the given imdefaultvalueId.
	 * @throws EntityNotFoundException If no ImDefaultValue is found.
     */
    ImDefaultValue getById(Integer imdefaultvalueId);

    /**
     * Find and return the ImDefaultValue by given id if exists, returns null otherwise.
     *
     * @param imdefaultvalueId The id of the ImDefaultValue to get; value cannot be null.
     * @return ImDefaultValue associated with the given imdefaultvalueId.
     */
    ImDefaultValue findById(Integer imdefaultvalueId);

	/**
     * Find and return the list of ImDefaultValues by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param imdefaultvalueIds The id's of the ImDefaultValue to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return ImDefaultValues associated with the given imdefaultvalueIds.
     */
    List<ImDefaultValue> findByMultipleIds(List<Integer> imdefaultvalueIds, boolean orderedReturn);

    /**
     * Find and return the ImDefaultValue for given company  andstateCode  andname  if exists.
     *
     * @param company value of company; value cannot be null.
     * @param stateCode value of stateCode; value cannot be null.
     * @param name value of name; value cannot be null.
     * @return ImDefaultValue associated with the given inputs.
     * @throws EntityNotFoundException if no matching ImDefaultValue found.
     */
    ImDefaultValue getByCompanyAndStateCodeAndName(String company, String stateCode, String name);

    /**
     * Updates the details of an existing ImDefaultValue. It replaces all fields of the existing ImDefaultValue with the given imDefaultValue.
     *
     * This method overrides the input field values using Server side or database managed properties defined on ImDefaultValue if any.
     *
     * @param imDefaultValue The details of the ImDefaultValue to be updated; value cannot be null.
     * @return The updated ImDefaultValue.
     * @throws EntityNotFoundException if no ImDefaultValue is found with given input.
     */
    ImDefaultValue update(@Valid ImDefaultValue imDefaultValue);

    /**
     * Deletes an existing ImDefaultValue with the given id.
     *
     * @param imdefaultvalueId The id of the ImDefaultValue to be deleted; value cannot be null.
     * @return The deleted ImDefaultValue.
     * @throws EntityNotFoundException if no ImDefaultValue found with the given id.
     */
    ImDefaultValue delete(Integer imdefaultvalueId);

    /**
     * Deletes an existing ImDefaultValue with the given object.
     *
     * @param imDefaultValue The instance of the ImDefaultValue to be deleted; value cannot be null.
     */
    void delete(ImDefaultValue imDefaultValue);

    /**
     * Find all ImDefaultValues matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching ImDefaultValues.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<ImDefaultValue> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all ImDefaultValues matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching ImDefaultValues.
     *
     * @see Pageable
     * @see Page
     */
    Page<ImDefaultValue> findAll(String query, Pageable pageable);

    /**
     * Exports all ImDefaultValues matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all ImDefaultValues matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the ImDefaultValues in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the ImDefaultValue.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}