/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.unirateservices.universal_rate.ImOldCarsInfo;


/**
 * ServiceImpl object for domain model class ImOldCarsInfo.
 *
 * @see ImOldCarsInfo
 */
@Service("UNIVERSAL_RATE.ImOldCarsInfoService")
@Validated
public class ImOldCarsInfoServiceImpl implements ImOldCarsInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImOldCarsInfoServiceImpl.class);


    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImOldCarsInfoDao")
    private WMGenericDao<ImOldCarsInfo, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<ImOldCarsInfo, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImOldCarsInfo create(ImOldCarsInfo imOldCarsInfo) {
        LOGGER.debug("Creating a new ImOldCarsInfo with information: {}", imOldCarsInfo);

        ImOldCarsInfo imOldCarsInfoCreated = this.wmGenericDao.create(imOldCarsInfo);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(imOldCarsInfoCreated);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImOldCarsInfo getById(Integer imoldcarsinfoId) {
        LOGGER.debug("Finding ImOldCarsInfo by id: {}", imoldcarsinfoId);
        return this.wmGenericDao.findById(imoldcarsinfoId);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImOldCarsInfo findById(Integer imoldcarsinfoId) {
        LOGGER.debug("Finding ImOldCarsInfo by id: {}", imoldcarsinfoId);
        try {
            return this.wmGenericDao.findById(imoldcarsinfoId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No ImOldCarsInfo found with id: {}", imoldcarsinfoId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public List<ImOldCarsInfo> findByMultipleIds(List<Integer> imoldcarsinfoIds, boolean orderedReturn) {
        LOGGER.debug("Finding ImOldCarsInfos by ids: {}", imoldcarsinfoIds);

        return this.wmGenericDao.findByMultipleIds(imoldcarsinfoIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImOldCarsInfo update(ImOldCarsInfo imOldCarsInfo) {
        LOGGER.debug("Updating ImOldCarsInfo with information: {}", imOldCarsInfo);

        this.wmGenericDao.update(imOldCarsInfo);
        this.wmGenericDao.refresh(imOldCarsInfo);

        return imOldCarsInfo;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImOldCarsInfo delete(Integer imoldcarsinfoId) {
        LOGGER.debug("Deleting ImOldCarsInfo with id: {}", imoldcarsinfoId);
        ImOldCarsInfo deleted = this.wmGenericDao.findById(imoldcarsinfoId);
        if (deleted == null) {
            LOGGER.debug("No ImOldCarsInfo found with id: {}", imoldcarsinfoId);
            throw new EntityNotFoundException(String.valueOf(imoldcarsinfoId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public void delete(ImOldCarsInfo imOldCarsInfo) {
        LOGGER.debug("Deleting ImOldCarsInfo with {}", imOldCarsInfo);
        this.wmGenericDao.delete(imOldCarsInfo);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImOldCarsInfo> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all ImOldCarsInfos");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImOldCarsInfo> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all ImOldCarsInfos");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service UNIVERSAL_RATE for table ImOldCarsInfo to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service UNIVERSAL_RATE for table ImOldCarsInfo to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}