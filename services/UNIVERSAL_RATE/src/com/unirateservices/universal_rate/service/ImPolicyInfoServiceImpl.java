/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.unirateservices.universal_rate.ImAddress;
import com.unirateservices.universal_rate.ImApplicant;
import com.unirateservices.universal_rate.ImCoverage;
import com.unirateservices.universal_rate.ImDriver;
import com.unirateservices.universal_rate.ImOldCarsInfo;
import com.unirateservices.universal_rate.ImPolicyInfo;
import com.unirateservices.universal_rate.ImPriorPolicyInfo;
import com.unirateservices.universal_rate.ImStateSpecificCoverage;
import com.unirateservices.universal_rate.ImVehicle;
import com.unirateservices.universal_rate.ImVehicleAssignment;
import com.unirateservices.universal_rate.ImVehicleCoverage;
import com.unirateservices.universal_rate.ImVehicleUse;


/**
 * ServiceImpl object for domain model class ImPolicyInfo.
 *
 * @see ImPolicyInfo
 */
@Service("UNIVERSAL_RATE.ImPolicyInfoService")
@Validated
public class ImPolicyInfoServiceImpl implements ImPolicyInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImPolicyInfoServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImAddressService")
    private ImAddressService imAddressService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImVehicleCoverageService")
    private ImVehicleCoverageService imVehicleCoverageService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImCoverageService")
    private ImCoverageService imCoverageService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImVehicleService")
    private ImVehicleService imVehicleService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImVehicleAssignmentService")
    private ImVehicleAssignmentService imVehicleAssignmentService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImVehicleUseService")
    private ImVehicleUseService imVehicleUseService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImApplicantService")
    private ImApplicantService imApplicantService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImPriorPolicyInfoService")
    private ImPriorPolicyInfoService imPriorPolicyInfoService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImDriverService")
    private ImDriverService imDriverService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImOldCarsInfoService")
    private ImOldCarsInfoService imOldCarsInfoService;

    @Lazy
    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImStateSpecificCoverageService")
    private ImStateSpecificCoverageService imStateSpecificCoverageService;

    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImPolicyInfoDao")
    private WMGenericDao<ImPolicyInfo, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<ImPolicyInfo, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImPolicyInfo create(ImPolicyInfo imPolicyInfo) {
        LOGGER.debug("Creating a new ImPolicyInfo with information: {}", imPolicyInfo);

        ImPolicyInfo imPolicyInfoCreated = this.wmGenericDao.create(imPolicyInfo);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(imPolicyInfoCreated);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImPolicyInfo getById(Integer impolicyinfoId) {
        LOGGER.debug("Finding ImPolicyInfo by id: {}", impolicyinfoId);
        return this.wmGenericDao.findById(impolicyinfoId);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImPolicyInfo findById(Integer impolicyinfoId) {
        LOGGER.debug("Finding ImPolicyInfo by id: {}", impolicyinfoId);
        try {
            return this.wmGenericDao.findById(impolicyinfoId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No ImPolicyInfo found with id: {}", impolicyinfoId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public List<ImPolicyInfo> findByMultipleIds(List<Integer> impolicyinfoIds, boolean orderedReturn) {
        LOGGER.debug("Finding ImPolicyInfos by ids: {}", impolicyinfoIds);

        return this.wmGenericDao.findByMultipleIds(impolicyinfoIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImPolicyInfo update(ImPolicyInfo imPolicyInfo) {
        LOGGER.debug("Updating ImPolicyInfo with information: {}", imPolicyInfo);

        this.wmGenericDao.update(imPolicyInfo);
        this.wmGenericDao.refresh(imPolicyInfo);

        return imPolicyInfo;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImPolicyInfo delete(Integer impolicyinfoId) {
        LOGGER.debug("Deleting ImPolicyInfo with id: {}", impolicyinfoId);
        ImPolicyInfo deleted = this.wmGenericDao.findById(impolicyinfoId);
        if (deleted == null) {
            LOGGER.debug("No ImPolicyInfo found with id: {}", impolicyinfoId);
            throw new EntityNotFoundException(String.valueOf(impolicyinfoId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public void delete(ImPolicyInfo imPolicyInfo) {
        LOGGER.debug("Deleting ImPolicyInfo with {}", imPolicyInfo);
        this.wmGenericDao.delete(imPolicyInfo);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImPolicyInfo> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all ImPolicyInfos");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImPolicyInfo> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all ImPolicyInfos");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service UNIVERSAL_RATE for table ImPolicyInfo to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service UNIVERSAL_RATE for table ImPolicyInfo to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImAddress> findAssociatedImAddresses(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imAddresses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imAddressService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImApplicant> findAssociatedImApplicants(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imApplicants");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imApplicantService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImCoverage> findAssociatedImCoverages(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imCoverages");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imCoverageService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImDriver> findAssociatedImDrivers(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imDrivers");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imDriverService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImOldCarsInfo> findAssociatedImOldCarsInfos(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imOldCarsInfos");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imOldCarsInfoService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImPriorPolicyInfo> findAssociatedImPriorPolicyInfos(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imPriorPolicyInfos");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imPriorPolicyInfoService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImStateSpecificCoverage> findAssociatedImStateSpecificCoverages(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imStateSpecificCoverages");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imStateSpecificCoverageService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImVehicle> findAssociatedImVehicles(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imVehicles");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imVehicleService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImVehicleCoverage> findAssociatedImVehicleCoverages(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imVehicleCoverages");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imVehicleCoverageService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImVehicleUse> findAssociatedImVehicleUses(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imVehicleUses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imVehicleUseService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImVehicleAssignment> findAssociatedImVehicleAssignments(Integer policyId, Pageable pageable) {
        LOGGER.debug("Fetching all associated imVehicleAssignments");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("imPolicyInfo.policyId = '" + policyId + "'");

        return imVehicleAssignmentService.findAll(queryBuilder.toString(), pageable);
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImAddressService instance
     */
    protected void setImAddressService(ImAddressService service) {
        this.imAddressService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImVehicleCoverageService instance
     */
    protected void setImVehicleCoverageService(ImVehicleCoverageService service) {
        this.imVehicleCoverageService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImCoverageService instance
     */
    protected void setImCoverageService(ImCoverageService service) {
        this.imCoverageService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImVehicleService instance
     */
    protected void setImVehicleService(ImVehicleService service) {
        this.imVehicleService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImVehicleAssignmentService instance
     */
    protected void setImVehicleAssignmentService(ImVehicleAssignmentService service) {
        this.imVehicleAssignmentService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImVehicleUseService instance
     */
    protected void setImVehicleUseService(ImVehicleUseService service) {
        this.imVehicleUseService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImApplicantService instance
     */
    protected void setImApplicantService(ImApplicantService service) {
        this.imApplicantService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImPriorPolicyInfoService instance
     */
    protected void setImPriorPolicyInfoService(ImPriorPolicyInfoService service) {
        this.imPriorPolicyInfoService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImDriverService instance
     */
    protected void setImDriverService(ImDriverService service) {
        this.imDriverService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImOldCarsInfoService instance
     */
    protected void setImOldCarsInfoService(ImOldCarsInfoService service) {
        this.imOldCarsInfoService = service;
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service ImStateSpecificCoverageService instance
     */
    protected void setImStateSpecificCoverageService(ImStateSpecificCoverageService service) {
        this.imStateSpecificCoverageService = service;
    }

}