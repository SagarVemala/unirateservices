/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.unirateservices.universal_rate.ImDropdownListItemBk04052019;
import com.unirateservices.universal_rate.ImDropdownListItemBk04052019Id;


/**
 * ServiceImpl object for domain model class ImDropdownListItemBk04052019.
 *
 * @see ImDropdownListItemBk04052019
 */
@Service("UNIVERSAL_RATE.ImDropdownListItemBk04052019Service")
@Validated
public class ImDropdownListItemBk04052019ServiceImpl implements ImDropdownListItemBk04052019Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImDropdownListItemBk04052019ServiceImpl.class);


    @Autowired
    @Qualifier("UNIVERSAL_RATE.ImDropdownListItemBk04052019Dao")
    private WMGenericDao<ImDropdownListItemBk04052019, ImDropdownListItemBk04052019Id> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<ImDropdownListItemBk04052019, ImDropdownListItemBk04052019Id> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImDropdownListItemBk04052019 create(ImDropdownListItemBk04052019 imDropdownListItemBk04052019) {
        LOGGER.debug("Creating a new ImDropdownListItemBk04052019 with information: {}", imDropdownListItemBk04052019);

        ImDropdownListItemBk04052019 imDropdownListItemBk04052019Created = this.wmGenericDao.create(imDropdownListItemBk04052019);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(imDropdownListItemBk04052019Created);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImDropdownListItemBk04052019 getById(ImDropdownListItemBk04052019Id imdropdownlistitembk04052019Id) {
        LOGGER.debug("Finding ImDropdownListItemBk04052019 by id: {}", imdropdownlistitembk04052019Id);
        return this.wmGenericDao.findById(imdropdownlistitembk04052019Id);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImDropdownListItemBk04052019 findById(ImDropdownListItemBk04052019Id imdropdownlistitembk04052019Id) {
        LOGGER.debug("Finding ImDropdownListItemBk04052019 by id: {}", imdropdownlistitembk04052019Id);
        try {
            return this.wmGenericDao.findById(imdropdownlistitembk04052019Id);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No ImDropdownListItemBk04052019 found with id: {}", imdropdownlistitembk04052019Id, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public List<ImDropdownListItemBk04052019> findByMultipleIds(List<ImDropdownListItemBk04052019Id> imdropdownlistitembk04052019Ids, boolean orderedReturn) {
        LOGGER.debug("Finding ImDropdownListItemBk04052019s by ids: {}", imdropdownlistitembk04052019Ids);

        return this.wmGenericDao.findByMultipleIds(imdropdownlistitembk04052019Ids, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImDropdownListItemBk04052019 update(ImDropdownListItemBk04052019 imDropdownListItemBk04052019) {
        LOGGER.debug("Updating ImDropdownListItemBk04052019 with information: {}", imDropdownListItemBk04052019);

        this.wmGenericDao.update(imDropdownListItemBk04052019);
        this.wmGenericDao.refresh(imDropdownListItemBk04052019);

        return imDropdownListItemBk04052019;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public ImDropdownListItemBk04052019 delete(ImDropdownListItemBk04052019Id imdropdownlistitembk04052019Id) {
        LOGGER.debug("Deleting ImDropdownListItemBk04052019 with id: {}", imdropdownlistitembk04052019Id);
        ImDropdownListItemBk04052019 deleted = this.wmGenericDao.findById(imdropdownlistitembk04052019Id);
        if (deleted == null) {
            LOGGER.debug("No ImDropdownListItemBk04052019 found with id: {}", imdropdownlistitembk04052019Id);
            throw new EntityNotFoundException(String.valueOf(imdropdownlistitembk04052019Id));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "UNIVERSAL_RATETransactionManager")
    @Override
    public void delete(ImDropdownListItemBk04052019 imDropdownListItemBk04052019) {
        LOGGER.debug("Deleting ImDropdownListItemBk04052019 with {}", imDropdownListItemBk04052019);
        this.wmGenericDao.delete(imDropdownListItemBk04052019);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImDropdownListItemBk04052019> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all ImDropdownListItemBk04052019s");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<ImDropdownListItemBk04052019> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all ImDropdownListItemBk04052019s");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service UNIVERSAL_RATE for table ImDropdownListItemBk04052019 to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service UNIVERSAL_RATE for table ImDropdownListItemBk04052019 to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "UNIVERSAL_RATETransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}