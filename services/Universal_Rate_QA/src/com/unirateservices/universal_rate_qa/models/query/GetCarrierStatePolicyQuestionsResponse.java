/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import com.wavemaker.runtime.data.annotations.ColumnAlias;

public class GetCarrierStatePolicyQuestionsResponse implements Serializable {


    @ColumnAlias("QUESTION_MASTER_ID")
    private Long questionMasterId;

    @ColumnAlias("CARRIER_ID")
    private Integer carrierId;

    @ColumnAlias("CARRIER_NAME")
    private String carrierName;

    @ColumnAlias("STATE_CD")
    private String stateCd;

    @ColumnAlias("EFF_DATE")
    private LocalDateTime effDate;

    @ColumnAlias("EXP_DATE")
    private LocalDateTime expDate;

    @ColumnAlias("QUESTION_TEXT")
    private String questionText;

    @ColumnAlias("ANSWER_DATA_TYPE")
    private String answerDataType;

    @ColumnAlias("ANSWER_TEXT")
    private String answerText;

    @ColumnAlias("DISPLAY_ORDER")
    private Integer displayOrder;

    @ColumnAlias("PRODUCT")
    private String product;

    @ColumnAlias("NODE_NAME")
    private String nodeName;

    @ColumnAlias("SHOW_HIDE_IND")
    private String showHideInd;

    @ColumnAlias("record_lock")
    private String recordLock;

    @ColumnAlias("mandatory_ind")
    private String mandatoryInd;

    public Long getQuestionMasterId() {
        return this.questionMasterId;
    }

    public void setQuestionMasterId(Long questionMasterId) {
        this.questionMasterId = questionMasterId;
    }

    public Integer getCarrierId() {
        return this.carrierId;
    }

    public void setCarrierId(Integer carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return this.carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getStateCd() {
        return this.stateCd;
    }

    public void setStateCd(String stateCd) {
        this.stateCd = stateCd;
    }

    public LocalDateTime getEffDate() {
        return this.effDate;
    }

    public void setEffDate(LocalDateTime effDate) {
        this.effDate = effDate;
    }

    public LocalDateTime getExpDate() {
        return this.expDate;
    }

    public void setExpDate(LocalDateTime expDate) {
        this.expDate = expDate;
    }

    public String getQuestionText() {
        return this.questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getAnswerDataType() {
        return this.answerDataType;
    }

    public void setAnswerDataType(String answerDataType) {
        this.answerDataType = answerDataType;
    }

    public String getAnswerText() {
        return this.answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Integer getDisplayOrder() {
        return this.displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getProduct() {
        return this.product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getNodeName() {
        return this.nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getShowHideInd() {
        return this.showHideInd;
    }

    public void setShowHideInd(String showHideInd) {
        this.showHideInd = showHideInd;
    }

    public String getRecordLock() {
        return this.recordLock;
    }

    public void setRecordLock(String recordLock) {
        this.recordLock = recordLock;
    }

    public String getMandatoryInd() {
        return this.mandatoryInd;
    }

    public void setMandatoryInd(String mandatoryInd) {
        this.mandatoryInd = mandatoryInd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetCarrierStatePolicyQuestionsResponse)) return false;
        final GetCarrierStatePolicyQuestionsResponse getCarrierStatePolicyQuestionsResponse = (GetCarrierStatePolicyQuestionsResponse) o;
        return Objects.equals(getQuestionMasterId(), getCarrierStatePolicyQuestionsResponse.getQuestionMasterId()) &&
                Objects.equals(getCarrierId(), getCarrierStatePolicyQuestionsResponse.getCarrierId()) &&
                Objects.equals(getCarrierName(), getCarrierStatePolicyQuestionsResponse.getCarrierName()) &&
                Objects.equals(getStateCd(), getCarrierStatePolicyQuestionsResponse.getStateCd()) &&
                Objects.equals(getEffDate(), getCarrierStatePolicyQuestionsResponse.getEffDate()) &&
                Objects.equals(getExpDate(), getCarrierStatePolicyQuestionsResponse.getExpDate()) &&
                Objects.equals(getQuestionText(), getCarrierStatePolicyQuestionsResponse.getQuestionText()) &&
                Objects.equals(getAnswerDataType(), getCarrierStatePolicyQuestionsResponse.getAnswerDataType()) &&
                Objects.equals(getAnswerText(), getCarrierStatePolicyQuestionsResponse.getAnswerText()) &&
                Objects.equals(getDisplayOrder(), getCarrierStatePolicyQuestionsResponse.getDisplayOrder()) &&
                Objects.equals(getProduct(), getCarrierStatePolicyQuestionsResponse.getProduct()) &&
                Objects.equals(getNodeName(), getCarrierStatePolicyQuestionsResponse.getNodeName()) &&
                Objects.equals(getShowHideInd(), getCarrierStatePolicyQuestionsResponse.getShowHideInd()) &&
                Objects.equals(getRecordLock(), getCarrierStatePolicyQuestionsResponse.getRecordLock()) &&
                Objects.equals(getMandatoryInd(), getCarrierStatePolicyQuestionsResponse.getMandatoryInd());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuestionMasterId(),
                getCarrierId(),
                getCarrierName(),
                getStateCd(),
                getEffDate(),
                getExpDate(),
                getQuestionText(),
                getAnswerDataType(),
                getAnswerText(),
                getDisplayOrder(),
                getProduct(),
                getNodeName(),
                getShowHideInd(),
                getRecordLock(),
                getMandatoryInd());
    }
}