/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateDropDownDefaultRequest implements Serializable {


    @JsonProperty("defaultValue")
    @NotNull
    private String defaultValue;

    @JsonProperty("itemId")
    @NotNull
    private String itemId;

    public String getDefaultValue() {
        return this.defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getItemId() {
        return this.itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateDropDownDefaultRequest)) return false;
        final UpdateDropDownDefaultRequest updateDropDownDefaultRequest = (UpdateDropDownDefaultRequest) o;
        return Objects.equals(getDefaultValue(), updateDropDownDefaultRequest.getDefaultValue()) &&
                Objects.equals(getItemId(), updateDropDownDefaultRequest.getItemId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDefaultValue(),
                getItemId());
    }
}