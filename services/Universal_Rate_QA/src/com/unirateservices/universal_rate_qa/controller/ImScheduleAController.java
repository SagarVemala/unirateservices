/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.commons.wrapper.StringWrapper;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.manager.ExportedFileManager;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.unirateservices.universal_rate_qa.ImScheduleA;
import com.unirateservices.universal_rate_qa.service.ImScheduleAService;


/**
 * Controller object for domain model class ImScheduleA.
 * @see ImScheduleA
 */
@RestController("Universal_Rate_QA.ImScheduleAController")
@Api(value = "ImScheduleAController", description = "Exposes APIs to work with ImScheduleA resource.")
@RequestMapping("/Universal_Rate_QA/ImScheduleA")
public class ImScheduleAController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImScheduleAController.class);

    @Autowired
	@Qualifier("Universal_Rate_QA.ImScheduleAService")
	private ImScheduleAService imScheduleAService;

	@Autowired
	private ExportedFileManager exportedFileManager;

	@ApiOperation(value = "Creates a new ImScheduleA instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ImScheduleA createImScheduleA(@RequestBody ImScheduleA imScheduleA) {
		LOGGER.debug("Create ImScheduleA with information: {}" , imScheduleA);

		imScheduleA = imScheduleAService.create(imScheduleA);
		LOGGER.debug("Created ImScheduleA with information: {}" , imScheduleA);

	    return imScheduleA;
	}

    @ApiOperation(value = "Returns the ImScheduleA instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ImScheduleA getImScheduleA(@PathVariable("id") Integer id) {
        LOGGER.debug("Getting ImScheduleA with id: {}" , id);

        ImScheduleA foundImScheduleA = imScheduleAService.getById(id);
        LOGGER.debug("ImScheduleA details with id: {}" , foundImScheduleA);

        return foundImScheduleA;
    }

    @ApiOperation(value = "Updates the ImScheduleA instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ImScheduleA editImScheduleA(@PathVariable("id") Integer id, @RequestBody ImScheduleA imScheduleA) {
        LOGGER.debug("Editing ImScheduleA with id: {}" , imScheduleA.getScheduleAId());

        imScheduleA.setScheduleAId(id);
        imScheduleA = imScheduleAService.update(imScheduleA);
        LOGGER.debug("ImScheduleA details with id: {}" , imScheduleA);

        return imScheduleA;
    }

    @ApiOperation(value = "Deletes the ImScheduleA instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteImScheduleA(@PathVariable("id") Integer id) {
        LOGGER.debug("Deleting ImScheduleA with id: {}" , id);

        ImScheduleA deletedImScheduleA = imScheduleAService.delete(id);

        return deletedImScheduleA != null;
    }

    /**
     * @deprecated Use {@link #findImScheduleAs(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of ImScheduleA instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ImScheduleA> searchImScheduleAsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering ImScheduleAs list by query filter:{}", (Object) queryFilters);
        return imScheduleAService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of ImScheduleA instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ImScheduleA> findImScheduleAs(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering ImScheduleAs list by filter:", query);
        return imScheduleAService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of ImScheduleA instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ImScheduleA> filterImScheduleAs(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering ImScheduleAs list by filter", query);
        return imScheduleAService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportImScheduleAs(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return imScheduleAService.export(exportType, query, pageable);
    }

    @ApiOperation(value = "Returns a URL to download a file for the data matching the optional query (q) request param and the required fields provided in the Export Options.") 
    @RequestMapping(value = "/export", method = {RequestMethod.POST}, consumes = "application/json")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public StringWrapper exportImScheduleAsAndGetURL(@RequestBody DataExportOptions exportOptions, Pageable pageable) {
        String exportedFileName = exportOptions.getFileName();
        if(exportedFileName == null || exportedFileName.isEmpty()) {
            exportedFileName = ImScheduleA.class.getSimpleName();
        }
        exportedFileName += exportOptions.getExportType().getExtension();
        String exportedUrl = exportedFileManager.registerAndGetURL(exportedFileName, outputStream -> imScheduleAService.export(exportOptions, pageable, outputStream));
        return new StringWrapper(exportedUrl);
    }

	@ApiOperation(value = "Returns the total count of ImScheduleA instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countImScheduleAs( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting ImScheduleAs");
		return imScheduleAService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getImScheduleAAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return imScheduleAService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service ImScheduleAService instance
	 */
	protected void setImScheduleAService(ImScheduleAService service) {
		this.imScheduleAService = service;
	}

}