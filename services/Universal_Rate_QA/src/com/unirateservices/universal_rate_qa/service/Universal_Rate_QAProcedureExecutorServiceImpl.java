/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.procedure.WMProcedureExecutor;

import com.unirateservices.universal_rate_qa.models.procedure.*;

@Service
public class Universal_Rate_QAProcedureExecutorServiceImpl implements Universal_Rate_QAProcedureExecutorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(Universal_Rate_QAProcedureExecutorServiceImpl.class);

    @Autowired
    @Qualifier("Universal_Rate_QAWMProcedureExecutor")
    private WMProcedureExecutor procedureExecutor;

    @Transactional(value = "Universal_Rate_QATransactionManager")
    @Override
    public CopyQuoteProcResponse executeCopyQuoteProc(Integer policyId, Integer newPolicyId) {
        Map<String, Object> params = new HashMap<>(2);

        params.put("policy_id", policyId);
        params.put("new_policy_id", newPolicyId);

        return procedureExecutor.executeNamedProcedure("CopyQuoteProc", params, CopyQuoteProcResponse.class);
    }

}