/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.unirateservices.universal_rate_qa.ImPageScript;

/**
 * Service object for domain model class {@link ImPageScript}.
 */
public interface ImPageScriptService {

    /**
     * Creates a new ImPageScript. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on ImPageScript if any.
     *
     * @param imPageScript Details of the ImPageScript to be created; value cannot be null.
     * @return The newly created ImPageScript.
     */
    ImPageScript create(@Valid ImPageScript imPageScript);


	/**
     * Returns ImPageScript by given id if exists.
     *
     * @param impagescriptId The id of the ImPageScript to get; value cannot be null.
     * @return ImPageScript associated with the given impagescriptId.
	 * @throws EntityNotFoundException If no ImPageScript is found.
     */
    ImPageScript getById(Integer impagescriptId);

    /**
     * Find and return the ImPageScript by given id if exists, returns null otherwise.
     *
     * @param impagescriptId The id of the ImPageScript to get; value cannot be null.
     * @return ImPageScript associated with the given impagescriptId.
     */
    ImPageScript findById(Integer impagescriptId);

	/**
     * Find and return the list of ImPageScripts by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param impagescriptIds The id's of the ImPageScript to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return ImPageScripts associated with the given impagescriptIds.
     */
    List<ImPageScript> findByMultipleIds(List<Integer> impagescriptIds, boolean orderedReturn);


    /**
     * Updates the details of an existing ImPageScript. It replaces all fields of the existing ImPageScript with the given imPageScript.
     *
     * This method overrides the input field values using Server side or database managed properties defined on ImPageScript if any.
     *
     * @param imPageScript The details of the ImPageScript to be updated; value cannot be null.
     * @return The updated ImPageScript.
     * @throws EntityNotFoundException if no ImPageScript is found with given input.
     */
    ImPageScript update(@Valid ImPageScript imPageScript);

    /**
     * Deletes an existing ImPageScript with the given id.
     *
     * @param impagescriptId The id of the ImPageScript to be deleted; value cannot be null.
     * @return The deleted ImPageScript.
     * @throws EntityNotFoundException if no ImPageScript found with the given id.
     */
    ImPageScript delete(Integer impagescriptId);

    /**
     * Deletes an existing ImPageScript with the given object.
     *
     * @param imPageScript The instance of the ImPageScript to be deleted; value cannot be null.
     */
    void delete(ImPageScript imPageScript);

    /**
     * Find all ImPageScripts matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching ImPageScripts.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<ImPageScript> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all ImPageScripts matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching ImPageScripts.
     *
     * @see Pageable
     * @see Page
     */
    Page<ImPageScript> findAll(String query, Pageable pageable);

    /**
     * Exports all ImPageScripts matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all ImPageScripts matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the ImPageScripts in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the ImPageScript.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}