/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.unirateservices.universal_rate_qa.ImEzlynxCase;
import com.unirateservices.universal_rate_qa.ImEzlynxCaseId;


/**
 * ServiceImpl object for domain model class ImEzlynxCase.
 *
 * @see ImEzlynxCase
 */
@Service("Universal_Rate_QA.ImEzlynxCaseService")
@Validated
public class ImEzlynxCaseServiceImpl implements ImEzlynxCaseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImEzlynxCaseServiceImpl.class);


    @Autowired
    @Qualifier("Universal_Rate_QA.ImEzlynxCaseDao")
    private WMGenericDao<ImEzlynxCase, ImEzlynxCaseId> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<ImEzlynxCase, ImEzlynxCaseId> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "Universal_Rate_QATransactionManager")
    @Override
    public ImEzlynxCase create(ImEzlynxCase imEzlynxCase) {
        LOGGER.debug("Creating a new ImEzlynxCase with information: {}", imEzlynxCase);

        ImEzlynxCase imEzlynxCaseCreated = this.wmGenericDao.create(imEzlynxCase);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(imEzlynxCaseCreated);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public ImEzlynxCase getById(ImEzlynxCaseId imezlynxcaseId) {
        LOGGER.debug("Finding ImEzlynxCase by id: {}", imezlynxcaseId);
        return this.wmGenericDao.findById(imezlynxcaseId);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public ImEzlynxCase findById(ImEzlynxCaseId imezlynxcaseId) {
        LOGGER.debug("Finding ImEzlynxCase by id: {}", imezlynxcaseId);
        try {
            return this.wmGenericDao.findById(imezlynxcaseId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No ImEzlynxCase found with id: {}", imezlynxcaseId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public List<ImEzlynxCase> findByMultipleIds(List<ImEzlynxCaseId> imezlynxcaseIds, boolean orderedReturn) {
        LOGGER.debug("Finding ImEzlynxCases by ids: {}", imezlynxcaseIds);

        return this.wmGenericDao.findByMultipleIds(imezlynxcaseIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "Universal_Rate_QATransactionManager")
    @Override
    public ImEzlynxCase update(ImEzlynxCase imEzlynxCase) {
        LOGGER.debug("Updating ImEzlynxCase with information: {}", imEzlynxCase);

        this.wmGenericDao.update(imEzlynxCase);
        this.wmGenericDao.refresh(imEzlynxCase);

        return imEzlynxCase;
    }

    @Transactional(value = "Universal_Rate_QATransactionManager")
    @Override
    public ImEzlynxCase delete(ImEzlynxCaseId imezlynxcaseId) {
        LOGGER.debug("Deleting ImEzlynxCase with id: {}", imezlynxcaseId);
        ImEzlynxCase deleted = this.wmGenericDao.findById(imezlynxcaseId);
        if (deleted == null) {
            LOGGER.debug("No ImEzlynxCase found with id: {}", imezlynxcaseId);
            throw new EntityNotFoundException(String.valueOf(imezlynxcaseId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "Universal_Rate_QATransactionManager")
    @Override
    public void delete(ImEzlynxCase imEzlynxCase) {
        LOGGER.debug("Deleting ImEzlynxCase with {}", imEzlynxCase);
        this.wmGenericDao.delete(imEzlynxCase);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public Page<ImEzlynxCase> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all ImEzlynxCases");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public Page<ImEzlynxCase> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all ImEzlynxCases");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service Universal_Rate_QA for table ImEzlynxCase to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service Universal_Rate_QA for table ImEzlynxCase to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}