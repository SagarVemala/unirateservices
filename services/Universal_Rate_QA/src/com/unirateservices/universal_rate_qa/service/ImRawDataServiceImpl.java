/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.unirateservices.universal_rate_qa.ImRawData;


/**
 * ServiceImpl object for domain model class ImRawData.
 *
 * @see ImRawData
 */
@Service("Universal_Rate_QA.ImRawDataService")
@Validated
public class ImRawDataServiceImpl implements ImRawDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImRawDataServiceImpl.class);


    @Autowired
    @Qualifier("Universal_Rate_QA.ImRawDataDao")
    private WMGenericDao<ImRawData, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<ImRawData, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "Universal_Rate_QATransactionManager")
    @Override
    public ImRawData create(ImRawData imRawData) {
        LOGGER.debug("Creating a new ImRawData with information: {}", imRawData);

        ImRawData imRawDataCreated = this.wmGenericDao.create(imRawData);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(imRawDataCreated);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public ImRawData getById(Integer imrawdataId) {
        LOGGER.debug("Finding ImRawData by id: {}", imrawdataId);
        return this.wmGenericDao.findById(imrawdataId);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public ImRawData findById(Integer imrawdataId) {
        LOGGER.debug("Finding ImRawData by id: {}", imrawdataId);
        try {
            return this.wmGenericDao.findById(imrawdataId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No ImRawData found with id: {}", imrawdataId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public List<ImRawData> findByMultipleIds(List<Integer> imrawdataIds, boolean orderedReturn) {
        LOGGER.debug("Finding ImRawDatas by ids: {}", imrawdataIds);

        return this.wmGenericDao.findByMultipleIds(imrawdataIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "Universal_Rate_QATransactionManager")
    @Override
    public ImRawData update(ImRawData imRawData) {
        LOGGER.debug("Updating ImRawData with information: {}", imRawData);

        this.wmGenericDao.update(imRawData);
        this.wmGenericDao.refresh(imRawData);

        return imRawData;
    }

    @Transactional(value = "Universal_Rate_QATransactionManager")
    @Override
    public ImRawData delete(Integer imrawdataId) {
        LOGGER.debug("Deleting ImRawData with id: {}", imrawdataId);
        ImRawData deleted = this.wmGenericDao.findById(imrawdataId);
        if (deleted == null) {
            LOGGER.debug("No ImRawData found with id: {}", imrawdataId);
            throw new EntityNotFoundException(String.valueOf(imrawdataId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "Universal_Rate_QATransactionManager")
    @Override
    public void delete(ImRawData imRawData) {
        LOGGER.debug("Deleting ImRawData with {}", imRawData);
        this.wmGenericDao.delete(imRawData);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public Page<ImRawData> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all ImRawDatas");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public Page<ImRawData> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all ImRawDatas");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service Universal_Rate_QA for table ImRawData to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service Universal_Rate_QA for table ImRawData to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "Universal_Rate_QATransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}