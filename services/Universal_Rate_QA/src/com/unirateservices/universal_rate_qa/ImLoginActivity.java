/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ImLoginActivity generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`IM_LOGIN_ACTIVITY`")
public class ImLoginActivity implements Serializable {

    private Long loginActivityId;
    private String userid;
    private LocalDateTime loginTimestamp;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`LOGIN_ACTIVITY_ID`", nullable = false, scale = 0, precision = 19)
    public Long getLoginActivityId() {
        return this.loginActivityId;
    }

    public void setLoginActivityId(Long loginActivityId) {
        this.loginActivityId = loginActivityId;
    }

    @Column(name = "`USERID`", nullable = true, length = 30)
    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Column(name = "`LOGIN_TIMESTAMP`", nullable = true)
    public LocalDateTime getLoginTimestamp() {
        return this.loginTimestamp;
    }

    public void setLoginTimestamp(LocalDateTime loginTimestamp) {
        this.loginTimestamp = loginTimestamp;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImLoginActivity)) return false;
        final ImLoginActivity imLoginActivity = (ImLoginActivity) o;
        return Objects.equals(getLoginActivityId(), imLoginActivity.getLoginActivityId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLoginActivityId());
    }
}