/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ImCarrierPrompt generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`IM_CARRIER_PROMPT`")
public class ImCarrierPrompt implements Serializable {

    private Integer carrierPromptId;
    private String state;
    private String carrierId;
    private String name;
    private String value;
    private LocalDateTime insertTimestamp;
    private LocalDateTime updateTimestamp;
    private String updatedBy;
    private String insertedBy;
    private Integer versionNum;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`CARRIER_PROMPT_ID`", nullable = false, scale = 0, precision = 10)
    public Integer getCarrierPromptId() {
        return this.carrierPromptId;
    }

    public void setCarrierPromptId(Integer carrierPromptId) {
        this.carrierPromptId = carrierPromptId;
    }

    @Column(name = "`STATE`", nullable = true, length = 2)
    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "`CARRIER_ID`", nullable = true, length = 5)
    public String getCarrierId() {
        return this.carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    @Column(name = "`NAME`", nullable = true, length = 100)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "`VALUE`", nullable = true, length = 100)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "`INSERT_TIMESTAMP`", nullable = true)
    public LocalDateTime getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(LocalDateTime insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    @Column(name = "`UPDATE_TIMESTAMP`", nullable = true)
    public LocalDateTime getUpdateTimestamp() {
        return this.updateTimestamp;
    }

    public void setUpdateTimestamp(LocalDateTime updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    @Column(name = "`UPDATED_BY`", nullable = true, length = 50)
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "`INSERTED_BY`", nullable = true, length = 50)
    public String getInsertedBy() {
        return this.insertedBy;
    }

    public void setInsertedBy(String insertedBy) {
        this.insertedBy = insertedBy;
    }

    @Column(name = "`VERSION_NUM`", nullable = true, scale = 0, precision = 10)
    public Integer getVersionNum() {
        return this.versionNum;
    }

    public void setVersionNum(Integer versionNum) {
        this.versionNum = versionNum;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImCarrierPrompt)) return false;
        final ImCarrierPrompt imCarrierPrompt = (ImCarrierPrompt) o;
        return Objects.equals(getCarrierPromptId(), imCarrierPrompt.getCarrierPromptId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCarrierPromptId());
    }
}