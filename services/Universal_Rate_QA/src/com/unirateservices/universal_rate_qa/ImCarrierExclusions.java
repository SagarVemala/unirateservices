/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ImCarrierExclusions generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`IM_CARRIER_EXCLUSIONS`", uniqueConstraints = {
            @UniqueConstraint(name = "`BROKER_CARRIER_EXCLUSIONS_POLNUM_SEQNUM_UK`", columnNames = {"`POL_NUM`", "`POL_SEQ_NUM`"})})
public class ImCarrierExclusions implements Serializable {

    private Integer carrierExclusionsId;
    private String polNum;
    private Integer polSeqNum;
    private String credit;
    private String pfm;
    private Integer carrierId;
    private String sendToCarrier;
    private LocalDateTime insertTimestamp;
    private LocalDateTime updateTimestamp;
    private String updatedBy;
    private String insertedBy;
    private Integer versionNum;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`CARRIER_EXCLUSIONS_ID`", nullable = false, scale = 0, precision = 10)
    public Integer getCarrierExclusionsId() {
        return this.carrierExclusionsId;
    }

    public void setCarrierExclusionsId(Integer carrierExclusionsId) {
        this.carrierExclusionsId = carrierExclusionsId;
    }

    @Column(name = "`POL_NUM`", nullable = true, length = 50)
    public String getPolNum() {
        return this.polNum;
    }

    public void setPolNum(String polNum) {
        this.polNum = polNum;
    }

    @Column(name = "`POL_SEQ_NUM`", nullable = true, scale = 0, precision = 10)
    public Integer getPolSeqNum() {
        return this.polSeqNum;
    }

    public void setPolSeqNum(Integer polSeqNum) {
        this.polSeqNum = polSeqNum;
    }

    @Column(name = "`CREDIT`", nullable = true, length = 50)
    public String getCredit() {
        return this.credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    @Column(name = "`PFM`", nullable = true, length = 50)
    public String getPfm() {
        return this.pfm;
    }

    public void setPfm(String pfm) {
        this.pfm = pfm;
    }

    @Column(name = "`CARRIER_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getCarrierId() {
        return this.carrierId;
    }

    public void setCarrierId(Integer carrierId) {
        this.carrierId = carrierId;
    }

    @Column(name = "`SEND_TO_CARRIER`", nullable = true, length = 50)
    public String getSendToCarrier() {
        return this.sendToCarrier;
    }

    public void setSendToCarrier(String sendToCarrier) {
        this.sendToCarrier = sendToCarrier;
    }

    @Column(name = "`INSERT_TIMESTAMP`", nullable = true)
    public LocalDateTime getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(LocalDateTime insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    @Column(name = "`UPDATE_TIMESTAMP`", nullable = true)
    public LocalDateTime getUpdateTimestamp() {
        return this.updateTimestamp;
    }

    public void setUpdateTimestamp(LocalDateTime updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    @Column(name = "`UPDATED_BY`", nullable = true, length = 50)
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "`INSERTED_BY`", nullable = true, length = 50)
    public String getInsertedBy() {
        return this.insertedBy;
    }

    public void setInsertedBy(String insertedBy) {
        this.insertedBy = insertedBy;
    }

    @Column(name = "`VERSION_NUM`", nullable = true, scale = 0, precision = 10)
    public Integer getVersionNum() {
        return this.versionNum;
    }

    public void setVersionNum(Integer versionNum) {
        this.versionNum = versionNum;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImCarrierExclusions)) return false;
        final ImCarrierExclusions imCarrierExclusions = (ImCarrierExclusions) o;
        return Objects.equals(getCarrierExclusionsId(), imCarrierExclusions.getCarrierExclusionsId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCarrierExclusionsId());
    }
}