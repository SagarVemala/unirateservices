/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate_qa;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * ImDriver generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`IM_DRIVER`")
public class ImDriver implements Serializable {

    private Integer driverId;
    private Integer policyId;
    private String prefix;
    private String firstName;
    private String middleName;
    private String lastName;
    private String suffix;
    private String gender;
    private String dob;
    private String ssn;
    private String driverLicenseNumber;
    private Integer sdipPoint;
    private String driverLicenseState;
    private String dateLicensed;
    private String driverLicenseStatus;
    private Integer ageLicensed;
    private String accPrev;
    private String maritalStatus;
    private String relation;
    private String industry;
    private String occupation;
    private String goodStudent;
    private String student100;
    private String driverTraining;
    private String goodDriver;
    private String matDriver;
    private Integer principalVehicle;
    private String rated;
    private String sr22;
    private String fr44;
    private String licenseRevokedSuspended;
    private LocalDateTime insertTimestamp;
    private LocalDateTime updateTimestamp;
    private String updatedBy;
    private String insertedBy;
    private Integer versionNum;
    private ImPolicyInfo imPolicyInfo;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`DRIVER_ID`", nullable = false, scale = 0, precision = 10)
    public Integer getDriverId() {
        return this.driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Column(name = "`POLICY_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getPolicyId() {
        return this.policyId;
    }

    public void setPolicyId(Integer policyId) {
        this.policyId = policyId;
    }

    @Column(name = "`PREFIX`", nullable = true, length = 10)
    public String getPrefix() {
        return this.prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Column(name = "`FIRST_NAME`", nullable = true, length = 64)
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "`MIDDLE_NAME`", nullable = true, length = 20)
    public String getMiddleName() {
        return this.middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Column(name = "`LAST_NAME`", nullable = true, length = 64)
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "`SUFFIX`", nullable = true, length = 10)
    public String getSuffix() {
        return this.suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Column(name = "`GENDER`", nullable = true, length = 10)
    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "`DOB`", nullable = true, length = 10)
    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Column(name = "`SSN`", nullable = true, length = 25)
    public String getSsn() {
        return this.ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    @Column(name = "`DRIVER_LICENSE_NUMBER`", nullable = true, length = 20)
    public String getDriverLicenseNumber() {
        return this.driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    @Column(name = "`SDIP_POINT`", nullable = true, scale = 0, precision = 10)
    public Integer getSdipPoint() {
        return this.sdipPoint;
    }

    public void setSdipPoint(Integer sdipPoint) {
        this.sdipPoint = sdipPoint;
    }

    @Column(name = "`DRIVER_LICENSE_STATE`", nullable = true, length = 2)
    public String getDriverLicenseState() {
        return this.driverLicenseState;
    }

    public void setDriverLicenseState(String driverLicenseState) {
        this.driverLicenseState = driverLicenseState;
    }

    @Column(name = "`DATE_LICENSED`", nullable = true, length = 10)
    public String getDateLicensed() {
        return this.dateLicensed;
    }

    public void setDateLicensed(String dateLicensed) {
        this.dateLicensed = dateLicensed;
    }

    @Column(name = "`DRIVER_LICENSE_STATUS`", nullable = true, length = 25)
    public String getDriverLicenseStatus() {
        return this.driverLicenseStatus;
    }

    public void setDriverLicenseStatus(String driverLicenseStatus) {
        this.driverLicenseStatus = driverLicenseStatus;
    }

    @Column(name = "`AGE_LICENSED`", nullable = true, scale = 0, precision = 10)
    public Integer getAgeLicensed() {
        return this.ageLicensed;
    }

    public void setAgeLicensed(Integer ageLicensed) {
        this.ageLicensed = ageLicensed;
    }

    @Column(name = "`ACC_PREV`", nullable = true, length = 10)
    public String getAccPrev() {
        return this.accPrev;
    }

    public void setAccPrev(String accPrev) {
        this.accPrev = accPrev;
    }

    @Column(name = "`MARITAL_STATUS`", nullable = true, length = 20)
    public String getMaritalStatus() {
        return this.maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Column(name = "`RELATION`", nullable = true, length = 20)
    public String getRelation() {
        return this.relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    @Column(name = "`INDUSTRY`", nullable = true, length = 50)
    public String getIndustry() {
        return this.industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    @Column(name = "`OCCUPATION`", nullable = true, length = 100)
    public String getOccupation() {
        return this.occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Column(name = "`GOOD_STUDENT`", nullable = true, length = 5)
    public String getGoodStudent() {
        return this.goodStudent;
    }

    public void setGoodStudent(String goodStudent) {
        this.goodStudent = goodStudent;
    }

    @Column(name = "`STUDENT100`", nullable = true, length = 5)
    public String getStudent100() {
        return this.student100;
    }

    public void setStudent100(String student100) {
        this.student100 = student100;
    }

    @Column(name = "`DRIVER_TRAINING`", nullable = true, length = 5)
    public String getDriverTraining() {
        return this.driverTraining;
    }

    public void setDriverTraining(String driverTraining) {
        this.driverTraining = driverTraining;
    }

    @Column(name = "`GOOD_DRIVER`", nullable = true, length = 5)
    public String getGoodDriver() {
        return this.goodDriver;
    }

    public void setGoodDriver(String goodDriver) {
        this.goodDriver = goodDriver;
    }

    @Column(name = "`MAT_DRIVER`", nullable = true, length = 5)
    public String getMatDriver() {
        return this.matDriver;
    }

    public void setMatDriver(String matDriver) {
        this.matDriver = matDriver;
    }

    @Column(name = "`PRINCIPAL_VEHICLE`", nullable = true, scale = 0, precision = 10)
    public Integer getPrincipalVehicle() {
        return this.principalVehicle;
    }

    public void setPrincipalVehicle(Integer principalVehicle) {
        this.principalVehicle = principalVehicle;
    }

    @Column(name = "`RATED`", nullable = true, length = 25)
    public String getRated() {
        return this.rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    @Column(name = "`SR22`", nullable = true, length = 5)
    public String getSr22() {
        return this.sr22;
    }

    public void setSr22(String sr22) {
        this.sr22 = sr22;
    }

    @Column(name = "`FR44`", nullable = true, length = 5)
    public String getFr44() {
        return this.fr44;
    }

    public void setFr44(String fr44) {
        this.fr44 = fr44;
    }

    @Column(name = "`LICENSE_REVOKED_SUSPENDED`", nullable = true, length = 5)
    public String getLicenseRevokedSuspended() {
        return this.licenseRevokedSuspended;
    }

    public void setLicenseRevokedSuspended(String licenseRevokedSuspended) {
        this.licenseRevokedSuspended = licenseRevokedSuspended;
    }

    @Column(name = "`INSERT_TIMESTAMP`", nullable = true)
    public LocalDateTime getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(LocalDateTime insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    @Column(name = "`UPDATE_TIMESTAMP`", nullable = true)
    public LocalDateTime getUpdateTimestamp() {
        return this.updateTimestamp;
    }

    public void setUpdateTimestamp(LocalDateTime updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    @Column(name = "`UPDATED_BY`", nullable = true, length = 50)
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "`INSERTED_BY`", nullable = true, length = 50)
    public String getInsertedBy() {
        return this.insertedBy;
    }

    public void setInsertedBy(String insertedBy) {
        this.insertedBy = insertedBy;
    }

    @Column(name = "`VERSION_NUM`", nullable = true, scale = 0, precision = 10)
    public Integer getVersionNum() {
        return this.versionNum;
    }

    public void setVersionNum(Integer versionNum) {
        this.versionNum = versionNum;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`POLICY_ID`", referencedColumnName = "`POLICY_ID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_DRIVER_POLICY`"))
    @Fetch(FetchMode.JOIN)
    public ImPolicyInfo getImPolicyInfo() {
        return this.imPolicyInfo;
    }

    public void setImPolicyInfo(ImPolicyInfo imPolicyInfo) {
        if(imPolicyInfo != null) {
            this.policyId = imPolicyInfo.getPolicyId();
        }

        this.imPolicyInfo = imPolicyInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImDriver)) return false;
        final ImDriver imDriver = (ImDriver) o;
        return Objects.equals(getDriverId(), imDriver.getDriverId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDriverId());
    }
}