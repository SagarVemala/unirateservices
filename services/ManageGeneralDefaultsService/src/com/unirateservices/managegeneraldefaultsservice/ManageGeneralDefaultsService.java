/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.managegeneraldefaultsservice;

import javax.servlet.http.HttpServletRequest;

import com.unirateservices.managegeneraldefaultsservice.model.UpdateGeneralDefaultsRequest;
import com.unirateservices.universal_rate.models.query.UpdateDropDownDefaultRequest;
import com.unirateservices.universal_rate.service.UNIVERSAL_RATEQueryExecutorService;
import com.unirateservices.universalraterutilsservice.UniversalRaterUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

import java.util.List;

//import com.unirateservices.managegeneraldefaultsservice.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class ManageGeneralDefaultsService {

    private static final Logger logger = LoggerFactory.getLogger(ManageGeneralDefaultsService.class);

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UNIVERSAL_RATEQueryExecutorService universal_rateQueryExecutorService;

    @Autowired
    private UniversalRaterUtilsService universalRaterUtilsService;


    /**
     * This is sample java operation that accepts an input from the caller and responds with "Hello".
     *
     * SecurityService that is Autowired will provide access to the security context of the caller. It has methods like isAuthenticated(),
     * getUserName() and getUserId() etc which returns the information based on the caller context.
     *
     * Methods in this class can declare HttpServletRequest, HttpServletResponse as input parameters to access the
     * caller's request/response objects respectively. These parameters will be injected when request is made (during API invocation).
     */

    public void updateDefault(List<UpdateGeneralDefaultsRequest> updateGeneralDefaultsRequests) {
        for(UpdateGeneralDefaultsRequest updateGeneralDefaultsRequest: updateGeneralDefaultsRequests ){
            // Creating Request Object
            UpdateDropDownDefaultRequest updateDropDownDefaultRequest=new UpdateDropDownDefaultRequest();
            updateDropDownDefaultRequest.setItemId(updateGeneralDefaultsRequest.getListItemId());
            updateDropDownDefaultRequest.setDefaultValue(updateGeneralDefaultsRequest.getDefaultValue());
            // Updating values
            universal_rateQueryExecutorService.executeUpdateDropDownDefault(updateDropDownDefaultRequest);
            logger.info("GENERAL_DEFAULT_UPDATE|services/ManageGeneralDefaultsService/src/com/unirateservices/managegeneraldefaultsservice/ManageGeneralDefaultsService.java:61|{}|{}|{}|General Default updated",securityService.getUserId(),updateGeneralDefaultsRequest.getListItemId(),updateGeneralDefaultsRequest.getDefaultValue() );
        }
    }

}
