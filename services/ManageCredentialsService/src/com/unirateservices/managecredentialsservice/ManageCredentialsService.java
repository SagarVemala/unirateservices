/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.managecredentialsservice;

import javax.servlet.http.HttpServletRequest;

import com.unirateservices.managecredentialsservice.model.UpdateCredentialsRequest;
import com.unirateservices.universal_rate.models.query.UpdateAnswerTextByIdRequest;
import com.unirateservices.universal_rate.models.query.UpdateAnswerTextRequest;
import com.unirateservices.universal_rate.service.UNIVERSAL_RATEQueryExecutorService;
import com.unirateservices.universalraterutilsservice.UniversalRaterUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

//import com.unirateservices.managecredentialsservice.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class ManageCredentialsService {

    private static final Logger logger = LoggerFactory.getLogger(ManageCredentialsService.class);

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UNIVERSAL_RATEQueryExecutorService universal_rateQueryExecutorService;
    
    @Autowired
    private UniversalRaterUtilsService universalRaterUtilsService;

    /**
     * This is sample java operation that accepts an input from the caller and responds with "Hello".
     *
     * SecurityService that is Autowired will provide access to the security context of the caller. It has methods like isAuthenticated(),
     * getUserName() and getUserId() etc which returns the information based on the caller context.
     *
     * Methods in this class can declare HttpServletRequest, HttpServletResponse as input parameters to access the
     * caller's request/response objects respectively. These parameters will be injected when request is made (during API invocation).
     */
    public void updateCredentials(UpdateCredentialsRequest updateCredentialsRequest) {
       if(updateCredentialsRequest.getBulk().equalsIgnoreCase("Yes")) {
           bulkUpdate(updateCredentialsRequest);
       }else{
           update(updateCredentialsRequest);
       }
    } 

    // Method to update each row irrespective of STATE
    private void bulkUpdate(UpdateCredentialsRequest updateCredentialsRequest){
        UpdateAnswerTextRequest updateAnswerTextRequest=new UpdateAnswerTextRequest();
        updateAnswerTextRequest.setCarrierId(updateCredentialsRequest.getCarrierID());
        
        // Update Username
        updateAnswerTextRequest.setNode("username");
        updateAnswerTextRequest.setValue(updateCredentialsRequest.getUsername());
        universal_rateQueryExecutorService.executeUpdateAnswerText(updateAnswerTextRequest);
        logger.info("CREDENTIAL_UPDATE|services/ManageCredentialsService/src/com/unirateservices/managecredentialsservice/ManageCredentialsService.java:67|{}|{}|Credentials updated",securityService.getUserId(),updateCredentialsRequest.getCarrierID() );
        // Update Password
        updateAnswerTextRequest.setNode("password");
        updateAnswerTextRequest.setValue(updateCredentialsRequest.getPassword());
        universal_rateQueryExecutorService.executeUpdateAnswerText(updateAnswerTextRequest);
        logger.info("CREDENTIAL_UPDATE|services/ManageCredentialsService/src/com/unirateservices/managecredentialsservice/ManageCredentialsService.java:67|{}|{}|Credentials updated",securityService.getUserId(),updateCredentialsRequest.getCarrierID() );
        // Update Agencycode
        updateAnswerTextRequest.setNode("agencycode");
        updateAnswerTextRequest.setValue(updateCredentialsRequest.getAgencyCode());
        universal_rateQueryExecutorService.executeUpdateAnswerText(updateAnswerTextRequest);
        logger.info("CREDENTIAL_UPDATE|services/ManageCredentialsService/src/com/unirateservices/managecredentialsservice/ManageCredentialsService.java:67|{}|{}|Credentials updated",securityService.getUserId(),updateCredentialsRequest.getCarrierID() );
    }

    // Method to update each row based on QUESTION_MASTER_ID
    private void update(UpdateCredentialsRequest updateCredentialsRequest){
        UpdateAnswerTextByIdRequest updateAnswerTextByIdRequest=new UpdateAnswerTextByIdRequest();

        // Update Username
        updateAnswerTextByIdRequest.setQuestionId(updateCredentialsRequest.getUsernameID());
        updateAnswerTextByIdRequest.setValue(updateCredentialsRequest.getUsername());
        universal_rateQueryExecutorService.executeUpdateAnswerTextById(updateAnswerTextByIdRequest);
        logger.info("CREDENTIAL_UPDATE|services/ManageCredentialsService/src/com/unirateservices/managecredentialsservice/ManageCredentialsService.java:90|{}|{}|Credentials updated",securityService.getUserId(),updateCredentialsRequest.getUsernameID() );
        // Update Password
        updateAnswerTextByIdRequest.setQuestionId(updateCredentialsRequest.getPasswordID());
        updateAnswerTextByIdRequest.setValue(updateCredentialsRequest.getPassword());
        universal_rateQueryExecutorService.executeUpdateAnswerTextById(updateAnswerTextByIdRequest);
        logger.info("CREDENTIAL_UPDATE|services/ManageCredentialsService/src/com/unirateservices/managecredentialsservice/ManageCredentialsService.java:90|{}|{}|Credentials updated",securityService.getUserId(),updateCredentialsRequest.getPasswordID() );
        // Update Agencycode
        updateAnswerTextByIdRequest.setQuestionId(updateCredentialsRequest.getAgencyCodeID());
        updateAnswerTextByIdRequest.setValue(updateCredentialsRequest.getAgencyCode());
        universal_rateQueryExecutorService.executeUpdateAnswerTextById(updateAnswerTextByIdRequest);
        logger.info("CREDENTIAL_UPDATE|services/ManageCredentialsService/src/com/unirateservices/managecredentialsservice/ManageCredentialsService.java:90|{}|{}|Credentials updated",securityService.getUserId(),updateCredentialsRequest.getAgencyCodeID() );
    }

}
