/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;

import java.util.List;



public class UpdateCarrierQuestionAnswersRequest {

    ImQuestionMaster question;

    List<ImQuestionAnswerLookup> currentAnwers;

    List<ImQuestionAnswerLookup> newAnswers;
    
        String oldQuestion;

    public String getOldQuestion() {
        return oldQuestion;
    }

    public void setOldQuestion(String oldQuestion) {
        this.oldQuestion = oldQuestion;
    }

    public ImQuestionMaster getQuestion() {
        return question;
    }

    public void setQuestion(ImQuestionMaster question) {
        this.question = question;
    }

    public List<ImQuestionAnswerLookup> getCurrentAnwers() {
        return currentAnwers;
    }

    public void setCurrentAnwers(List<ImQuestionAnswerLookup> currentAnwers) {
        this.currentAnwers = currentAnwers;
    }

    public List<ImQuestionAnswerLookup> getNewAnswers() {
        return newAnswers;
    }

    public void setNewAnswers(List<ImQuestionAnswerLookup> newAnswers) {
        this.newAnswers = newAnswers;
    }
}