/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.carrieranswersservice;

import com.unirateservices.universal_rate.ImPolicyQuestions;
import com.unirateservices.universal_rate.models.query.GetAnswersForQuestionByStateResponse;
import com.unirateservices.universal_rate.service.ImPolicyQuestionsService;
import com.unirateservices.universal_rate.service.ImQuestionMasterService;
import com.unirateservices.universal_rate.service.ImQuestionAnswerLookupService;
import com.unirateservices.universal_rate.service.UNIVERSAL_RATEQueryExecutorService;
import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.unirateservices.universal_rate.ImQuestionMaster;
import com.unirateservices.universal_rate.ImQuestionAnswerLookup;
import com.unirateservices.universal_rate.models.query.GetDropDownValuesResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import  com.unirateservices.universal_rate.CreateQuestionAnswersRequest;
import com.unirateservices.universal_rate.UpdateCarrierQuestionAnswersRequest;
import  com.unirateservices.universal_rate.models.query.UpdateQuestionDefaultRequest;
import  com.unirateservices.universal_rate.models.query.UpdateAllStatesQuestionRequest;


//import com.unirateservices.carrieranswersservice.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class CarrierAnswersService {

    private static final Logger logger = LoggerFactory.getLogger(CarrierAnswersService.class);

    @Autowired
    private SecurityService securityService;

    @Autowired
    UNIVERSAL_RATEQueryExecutorService queryService;

    @Autowired
    ImPolicyQuestionsService imPolicyQuestionsService;
    
        @Autowired
        ImQuestionMasterService imQuestionMasterService;
  

    @Autowired
    ImQuestionAnswerLookupService imQuestionAnswerLookupService;

    /**
     * This is sample java operation that accepts an input from the caller and responds with "Hello".
     *
     * SecurityService that is Autowired will provide access to the security context of the caller. It has methods like isAuthenticated(),
     * getUserName() and getUserId() etc which returns the information based on the caller context.
     *
     * Methods in this class can declare HttpServletRequest, HttpServletResponse as input parameters to access the
     * caller's request/response objects respectively. These parameters will be injected when request is made (during API invocation).
     */
    public Map getAnswerBySate(String stateName, HttpServletRequest request) {

        Pageable pageable =  new PageRequest(0,Integer.MAX_VALUE);

        List<GetAnswersForQuestionByStateResponse> carrierAnswersList = queryService.executeGetAnswersForQuestionByState(stateName,pageable).getContent();

        Map<Long, List<GetAnswersForQuestionByStateResponse>> carrierAnswersMap = carrierAnswersList.stream().collect(Collectors.groupingBy(GetAnswersForQuestionByStateResponse::getQuestionMasterId));




        // for( Map.Entry<Long, List<GetAnswersForQuestionByStateResponse>> entry :carrierAnswersMap.entrySet()){
        //     logger.info("the question master id is" + entry.getKey().toString());

        //     List<GetAnswersForQuestionByStateResponse> value = entry.getValue();
        //     for (GetAnswersForQuestionByStateResponse response :value){
        //         logger.info("the question default ind is" + response.getDefaultInd());
        //         logger.info("the question answer key  is" + response.getAnswerKey());
        //         logger.info("the question answer val is" + response.getAnswerValue());
        //         //logger.info("the question answer id is" + response.getQuestionAnswerId());
        //         logger.info("the question master id is" + response.getQuestionMasterId());

        //     }


        // }
        return carrierAnswersMap;
    }

    public int bulkLoadPolicyAnswers(List<ImPolicyQuestions>  inputPolicyAnswers, String policyId){
        ImPolicyQuestions imPolicyQuestions= new ImPolicyQuestions() ;
        imPolicyQuestions.getQuestionMasterId();
        String query = "policyId =" + policyId;
        Pageable pageable =  new PageRequest(0,Integer.MAX_VALUE);
        List<ImPolicyQuestions> currentPolicyAnswers = imPolicyQuestionsService.findAll(query, pageable).getContent();

        Map<Long,Long> qmiPqMap = new HashMap<>();
        for(ImPolicyQuestions currentPolicyAnswer :currentPolicyAnswers){
            qmiPqMap.put(currentPolicyAnswer.getQuestionMasterId(),currentPolicyAnswer.getPolicyQuestionsId());
            logger.info("getQuestionMasterId  is " + currentPolicyAnswer.getQuestionMasterId());
            logger.info("getPolicyQuestionsId  is " + currentPolicyAnswer.getPolicyQuestionsId());
        }
        // List<Long> savedQuestionIds = currentPolicyAnswers.stream().map(ImPolicyQuestions::getQuestionMasterId).collect(Collectors.toList());


        // Map<Long,Long> qmiPqMap =currentPolicyAnswers.stream().collect(Collectors.toMap(ImPolicyQuestions::getQuestionAnswerId,ImPolicyQuestions::getPolicyQuestionsId));

        logger.info("qmiPqMap size is " + qmiPqMap.size());
        logger.info("inputPolicyAnswers size is " + inputPolicyAnswers.size());

        // currentPolicyAnswers.stream().map
        for(ImPolicyQuestions inputPolicyAnswer:inputPolicyAnswers){
            ImPolicyQuestions imPolicyQuestionsNew = new ImPolicyQuestions();
            inputPolicyAnswer.setPolicyId(Integer.parseInt(policyId));
            // if(savedQuestionIds.contains(inputPolicyAnswer.getQuestionMasterId())){
            logger.info("the qmi is"+inputPolicyAnswer.getQuestionMasterId());
            if(qmiPqMap.containsKey(inputPolicyAnswer.getQuestionMasterId())){

                imPolicyQuestionsNew.setPolicyId(inputPolicyAnswer.getPolicyId());
                imPolicyQuestionsNew.setPolicyQuestionsId(inputPolicyAnswer.getPolicyQuestionsId());
                imPolicyQuestionsNew.setAnswerText(inputPolicyAnswer.getAnswerText());
                imPolicyQuestionsNew.setQuestionAnswerId(inputPolicyAnswer.getQuestionAnswerId());
                imPolicyQuestionsNew.setQuestionMasterId(inputPolicyAnswer.getQuestionMasterId());


                logger.info(inputPolicyAnswer.getAnswerText());
                logger.info("qai is "+inputPolicyAnswer.getQuestionAnswerId());
                logger.info("qmi is "+inputPolicyAnswer.getQuestionMasterId());

                inputPolicyAnswer.setPolicyQuestionsId(qmiPqMap.get(inputPolicyAnswer.getQuestionMasterId()));
                logger.info("sqi is"+inputPolicyAnswer.getPolicyQuestionsId());
                imPolicyQuestionsService.update(inputPolicyAnswer);

                logger.info("updating request");
                // logger.info("size is " + savedQuestionIds.size());
                logger.info("size cpa is " +currentPolicyAnswers.size());
                //return 0;
            }
            else{
                imPolicyQuestionsNew.setPolicyId(inputPolicyAnswer.getPolicyId());
                //imPolicyQuestionsNew.setPolicyQuestionsId(inputPolicyAnswer.getPolicyQuestionsId());
                imPolicyQuestionsNew.setAnswerText(inputPolicyAnswer.getAnswerText());
                imPolicyQuestionsNew.setQuestionAnswerId(inputPolicyAnswer.getQuestionAnswerId());
                imPolicyQuestionsNew.setQuestionMasterId(inputPolicyAnswer.getQuestionMasterId());
                imPolicyQuestionsService.create(imPolicyQuestionsNew);
                logger.info("creating request");
                logger.info("size is " +currentPolicyAnswers.size());
                logger.info("size cpa is " +currentPolicyAnswers.size());
                    }
        }
        logger.info("qmiPqMap size is " + qmiPqMap.size());
        return qmiPqMap.size();
    }
    
    //     public void addCarrierQuestionsAndAnswers(ImQuestionMaster question , List<ImQuestionAnswerLookup> answers,String state){



    //     if(answers==null  ||  answers.size()==0){
    //         imQuestionMasterService.create(question);
    //     }else{
    //         if(state.equalsIgnoreCase("All")){
    //             Pageable pageable =  new PageRequest(0,Integer.MAX_VALUE);
    //             List<GetDropDownValuesResponse> statesResponse = queryService.executeGetDropDownValues("StateCodeType", pageable).getContent();

    //             //statesResponse.stream().
    //             for(GetDropDownValuesResponse getDropDownValuesResponse : statesResponse){
    //                 String stateCd=getDropDownValuesResponse.getListItemValue();
    //                 question.setStateCd(stateCd);
    //                 Long questionMasterId = imQuestionMasterService.create(question).getQuestionMasterId();
    //                 for(ImQuestionAnswerLookup answer:answers){
    //                     answer.setQuestionMasterId(questionMasterId);
    //                     imQuestionAnswerLookupService.create(answer);

    //                 }

    //             }
    //         }else{
    //             Long questionMasterId = imQuestionMasterService.create(question).getQuestionMasterId();
    //             for(ImQuestionAnswerLookup answer:answers){
    //                 answer.setQuestionMasterId(questionMasterId);
    //                 imQuestionAnswerLookupService.create(answer);

    //             }
    //         }

    //     }

    // }
    
     public String  addCarrierQuestionsAndAnswers(CreateQuestionAnswersRequest createQuestionAnswersRequest){

        List<ImQuestionAnswerLookup> answers = createQuestionAnswersRequest.getAnswers();
        ImQuestionMaster question = createQuestionAnswersRequest.getQuestion();
        String state = question.getStateCd();

        if(answers==null  ||  answers.size()==0){
            ImQuestionMaster imQuestionMaster= imQuestionMasterService.create(question);
            logger.info("CARRIER_QUE_INSERT|services/CarrierAnswersService/src/com/unirateservices/carrieranswersservice/CarrierAnswersService.java:206|{}|{}|Carrier Question Inserted",securityService.getUserId(),imQuestionMaster.getQuestionMasterId() );
        }else{
            if(state.equalsIgnoreCase("All")){
                Pageable pageable =  new PageRequest(0,Integer.MAX_VALUE);
                List<GetDropDownValuesResponse> statesResponse = queryService.executeGetDropDownValues("StateCodeType", pageable).getContent();
                logger.info("statez size is"+statesResponse.size());

                //statesResponse.stream().
                for(GetDropDownValuesResponse getDropDownValuesResponse : statesResponse){
                    String stateCd=getDropDownValuesResponse.getListItemValue();
                     logger.info("the state code set to new question is"+stateCd);
                    question.setStateCd(stateCd);
                    Long questionMasterId = imQuestionMasterService.create(question).getQuestionMasterId();
                    for(ImQuestionAnswerLookup answer:answers){
                        answer.setQuestionMasterId(questionMasterId);
                        imQuestionAnswerLookupService.create(answer);

                    }

                }
            }else{
                Long questionMasterId = imQuestionMasterService.create(question).getQuestionMasterId();
                for(ImQuestionAnswerLookup answer:answers){
                    answer.setQuestionMasterId(questionMasterId);
                    imQuestionAnswerLookupService.create(answer);

                }
            }

        }
        return "success";
    }
    
    
     public String  changeCarrierQuestionsNAnswers(UpdateCarrierQuestionAnswersRequest carrierUpdateRequest){
           ImQuestionMaster question =carrierUpdateRequest.getQuestion();
           logger.info("Number of answers to be edited"+carrierUpdateRequest.getCurrentAnwers().size());
           
           UpdateAllStatesQuestionRequest updateAllStatesQuestionRequest = new UpdateAllStatesQuestionRequest();


         updateAllStatesQuestionRequest.setQuestionText(question.getQuestionText());
           updateAllStatesQuestionRequest.setEffDate(question.getEffDate());
           updateAllStatesQuestionRequest.setDataType(question.getAnswerDataType());
           updateAllStatesQuestionRequest.setNodeName(question.getNodeName());
           updateAllStatesQuestionRequest.setCarrierListId(question.getCarrierListId());
           updateAllStatesQuestionRequest.setQuestionTextOld(carrierUpdateRequest.getOldQuestion());

         int questionsNumUpdated= queryService.executeUpdateAllStatesQuestion(updateAllStatesQuestionRequest);
         logger.info("Number of questions updated "+questionsNumUpdated);
          String query = "questionText= " + "'" +question.getQuestionText() +"'" ;
          Pageable pageable =  new PageRequest(0,Integer.MAX_VALUE);

        List<ImQuestionMaster> questions =imQuestionMasterService.findAll(query,pageable).getContent();

    for(ImQuestionMaster imQuestion:questions){
        //for each of the question matching text update/add new answers

         long qmi =imQuestion.getQuestionMasterId();

        String query1 = "questionMasterId =" + qmi;
        //long qmi = imQuestion.getQuestionMasterId();

        List<ImQuestionAnswerLookup> existingAnswers = imQuestionAnswerLookupService.findAll(query1, pageable).getContent();

 logger.info("Number of questions  answers that will be updated "+existingAnswers.size());
 
 logger.info("Number of answers to be edited"+carrierUpdateRequest.getCurrentAnwers().size());

       
            for(ImQuestionAnswerLookup inputAnswer:carrierUpdateRequest.getCurrentAnwers()){
                inputAnswer.setQuestionMasterId(qmi);
                imQuestionAnswerLookupService.create(inputAnswer);
            }
    // }
    
     for(ImQuestionAnswerLookup answer:existingAnswers){
         //remove old options after saving new ones
             imQuestionAnswerLookupService.delete(answer.getQuestionAnswerId());
     }
     for(ImQuestionAnswerLookup newAnswer:carrierUpdateRequest.getNewAnswers()){
            newAnswer.setQuestionMasterId(qmi);
             imQuestionAnswerLookupService.create(newAnswer);

     }
        logger.info("CARRIER_QUE_UPDATE|services/CarrierAnswersService/src/com/unirateservices/carrieranswersservice/CarrierAnswersService.java:248|{}|{}|Carrier Question updated",securityService.getUserId(),qmi );


    }

 return "Success";
      }




}
