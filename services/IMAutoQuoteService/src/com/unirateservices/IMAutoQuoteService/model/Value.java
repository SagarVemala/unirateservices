/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class Value {

    @XmlAttribute
    protected String nameNew;

    @XmlValue
    protected String valueNew;

    public String getNameNew() {
        return nameNew;
    }

    public void setNameNew(String name) {
        this.nameNew = name;
    }

    public String getValueNew() {
        return valueNew;
    }

    public void setValueNew(String value) {
        this.valueNew = value;
    }
}