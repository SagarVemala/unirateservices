/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlRootElement(name = "PropertyValues")
@XmlAccessorType(XmlAccessType.NONE)
// @XmlRootElement(name="PropertyValues")
public class PropertyValues {

    // @XmlAnyElement
    public ArrayList<PropertyValue> getValue() {
        return value;
    }

    public void setValue(ArrayList<PropertyValue> value) {
        this.value = value;
    }
    //@XmlAnyElement
      @XmlElement(name = "value", required = true)
    private ArrayList<PropertyValue> value = new ArrayList<PropertyValue>();



    @Override
    public String toString()
    {
        return "ClassPojo";
    }
}