/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;
import javax.xml.bind.annotation.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "OtherCollisionDeductible",
        "CollisionDeductible",
        "FullGlass",
        "TowingDeductible",
        "RentalDeductible",
        "LiabilityNotRequired",
        "LoanLeaseCoverage",
        "StatedAmount",
        "ReplacementCost"
})
public class VehicleCoverage {
    private String LoanLeaseCoverage;

    private String LiabilityNotRequired;

    private String ReplacementCost;

    private String TowingDeductible;

    private String FullGlass;

    @XmlAttribute(name="Id")
    private Integer Id;

    private String OtherCollisionDeductible;

    private String RentalDeductible;

    private String CollisionDeductible;

    private Integer StatedAmount;

    public String getLoanLeaseCoverage ()
    {
        return LoanLeaseCoverage;
    }

    public void setLoanLeaseCoverage (String LoanLeaseCoverage)
    {
        this.LoanLeaseCoverage = LoanLeaseCoverage;
    }

    public String getLiabilityNotRequired ()
    {
        return LiabilityNotRequired;
    }

    public void setLiabilityNotRequired (String LiabilityNotRequired)
    {
        this.LiabilityNotRequired = LiabilityNotRequired;
    }

    public String getReplacementCost ()
    {
        return ReplacementCost;
    }

    public void setReplacementCost (String ReplacementCost)
    {
        this.ReplacementCost = ReplacementCost;
    }

    public String getTowingDeductible ()
    {
        return TowingDeductible;
    }

    public void setTowingDeductible (String TowingDeductible)
    {
        this.TowingDeductible = TowingDeductible;
    }

    public String getFullGlass ()
    {
        return FullGlass;
    }

    public void setFullGlass (String FullGlass)
    {
        this.FullGlass = FullGlass;
    }

    public Integer getId ()
    {
        return Id;
    }

    public void setId (Integer Id)
    {
        this.Id = Id;
    }

    public String getOtherCollisionDeductible ()
    {
        return OtherCollisionDeductible;
    }

    public void setOtherCollisionDeductible (String OtherCollisionDeductible)
    {
        this.OtherCollisionDeductible = OtherCollisionDeductible;
    }

    public String getRentalDeductible ()
    {
        return RentalDeductible;
    }

    public void setRentalDeductible (String RentalDeductible)
    {
        this.RentalDeductible = RentalDeductible;
    }

    public String getCollisionDeductible ()
    {
        return CollisionDeductible;
    }

    public void setCollisionDeductible (String CollisionDeductible)
    {
        this.CollisionDeductible = CollisionDeductible;
    }

    public Integer getStatedAmount ()
    {
        return StatedAmount;
    }

    public void setStatedAmount (Integer StatedAmount)
    {
        this.StatedAmount = StatedAmount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LoanLeaseCoverage = "+LoanLeaseCoverage+", LiabilityNotRequired = "+LiabilityNotRequired+", ReplacementCost = "+ReplacementCost+", TowingDeductible = "+TowingDeductible+", FullGlass = "+FullGlass+", Id = "+Id+", OtherCollisionDeductible = "+OtherCollisionDeductible+", RentalDeductible = "+RentalDeductible+", CollisionDeductible = "+CollisionDeductible+", StatedAmount = "+StatedAmount+"]";
    }
}