/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PropertyValue")
public class PropertyValue {

    @XmlAttribute(name="name")
    private String propertyName;

    @XmlValue
    private String content;


    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propName) {
        this.propertyName = propName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [content = "+content+", Name = "+propertyName+"]";
    }
}