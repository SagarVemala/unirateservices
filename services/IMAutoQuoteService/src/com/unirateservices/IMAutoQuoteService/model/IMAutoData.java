/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;

import com.unirateservices.VehicleAssignment;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.namespace.QName;
import java.util.ArrayList; 
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "policyId",
        "ApplicantObject",
        "PriorPolicyInfo",
        "PolicyInfoObject",
        "ResidenceInfo",
        "Drivers",
        "Vehicle",
        "VehiclesUse",
        "Coverages",
        "VehicleAssignments",
        "GeneralInfoObject",
        "Carriers"
})
@XmlRootElement(name = "IMAUTO")
public class IMAutoData {
    public Integer getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Integer policyId) {
        this.policyId = policyId;
    }
    @XmlElement(name = "PolicyId", required = true)
    Integer policyId;
    @XmlElement(name = "Applicant", required = true)
    Applicant ApplicantObject = new Applicant();
    @XmlElement(name = "PriorPolicyInfo", required = true)
    PriorPolicyInfo PriorPolicyInfo = new PriorPolicyInfo();
    @XmlElement(name = "PolicyInfo", required = true)
    PolicyInfo PolicyInfoObject = new PolicyInfo();
    @XmlElement(name = "ResidenceInfo", required = true)
    ResidenceInfo ResidenceInfo = new ResidenceInfo();
    @XmlElement(name = "Drivers", required = true)
    Drivers Drivers = new Drivers();

    @XmlElement(name = "Vehicles", required = true)
    Vehicles Vehicle = new Vehicles();

    @XmlElement(name = "VehiclesUse", required = true)
    VehiclesUse VehiclesUse= new VehiclesUse();

    @XmlElement(name = "Coverages", required = true)
    Coverages Coverages = new Coverages();

    // @XmlElement(name = "VehicleAssignments", required = true)
    VehicleAssignments VehicleAssignments = new VehicleAssignments();

    @XmlElement(name = "GeneralInfo", required = true)
    GeneralInfo GeneralInfoObject = new GeneralInfo();
    // @XmlElement(name = "Carriers", required = true)
    Carriers Carriers = new Carriers();

    public Drivers getDrivers() {
        return Drivers;
    }

    public void setDrivers(Drivers driversObject) {
        Drivers = driversObject;
    }

    public Vehicles getVehicle() {
        return Vehicle;
    }

    public void setVehicle(Vehicles vehicle) {
        this.Vehicle = vehicle;
    }




    // Getter Methods

    public Applicant getApplicant() {
        return ApplicantObject;
    }

    public PriorPolicyInfo getPriorPolicyInfo() {
        return PriorPolicyInfo;
    }

    public PolicyInfo getPolicyInfo() {
        return PolicyInfoObject;
    }

    public ResidenceInfo getResidenceInfo() {
        return ResidenceInfo;
    }

//    public Drivers getDrivers() {
//        return DriversObject;
//    }

    public Vehicles getVehicles() {
        return Vehicle;
    }

    public VehiclesUse getVehiclesUse() {
        return VehiclesUse;
    }

    public Coverages getCoverages() {
        return Coverages;
    }

    public VehicleAssignments getVehicleAssignments() {
        return VehicleAssignments;
    }

    public GeneralInfo getGeneralInfo() {
        return GeneralInfoObject;
    }

    public Carriers getCarriers() {
        return Carriers;
    }

    // Setter Methods

    public void setApplicant(Applicant ApplicantObject) {
        this.ApplicantObject = ApplicantObject;
    }

    public void setPriorPolicyInfo(PriorPolicyInfo PriorPolicyInfo) {
        this.PriorPolicyInfo = PriorPolicyInfo;
    }

    public void setPolicyInfo(PolicyInfo PolicyInfoObject) {
        this.PolicyInfoObject = PolicyInfoObject;
    }

    public void setResidenceInfo(ResidenceInfo ResidenceInfoObject) {
        this.ResidenceInfo= ResidenceInfoObject;
    }

//    public void setDrivers(Drivers DriversObject) {
//        this.DriversObject = DriversObject;
//    }

    public void setVehicles(Vehicles VehiclesObject) {
        this.Vehicle = VehiclesObject;
    }

    public void setVehiclesUse(VehiclesUse VehiclesUse) {
        this.VehiclesUse = VehiclesUse;
    }

    public void setCoverages(Coverages Coverages) {
        this.Coverages = Coverages;
    }

    public void setVehicleAssignments(VehicleAssignments VehicleAssignments) {
        this.VehicleAssignments = VehicleAssignments;
    }

    public void setGeneralInfo(GeneralInfo GeneralInfoObject) {
        this.GeneralInfoObject = GeneralInfoObject;
    }

    public void setCarriers(Carriers Carriers) {
        this.Carriers = Carriers;
    }

    @XmlRootElement(name = "Carriers")
    public static class Carriers {
        public ArrayList<CarrierExecution> CarrierExecution = new ArrayList<CarrierExecution>();

        public ArrayList<CarrierExecution> getCarrierExecution ()
        {
            return CarrierExecution;
        }

        public void setCarrierExecution (ArrayList<CarrierExecution> CarrierExecution)
        {
            this.CarrierExecution = CarrierExecution;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [CarrierExecution = "+CarrierExecution+"]";
        }
    }




    public static class GeneralInfo {
        private String RatingStateCode;


        // Getter Methods

        public String getRatingStateCode() {
            return RatingStateCode;
        }

        // Setter Methods

        public void setRatingStateCode(String RatingStateCode) {
            this.RatingStateCode = RatingStateCode;
        }
    }

    public static class VehicleAssignments {
        ArrayList<VehicleAssignment> VehicleAssignment = new ArrayList<VehicleAssignment>();


        // Getter Methods

        public ArrayList<com.unirateservices.VehicleAssignment> getVehicleAssignment() {
            return VehicleAssignment;
        }

        public void setVehicleAssignment(ArrayList<com.unirateservices.VehicleAssignment> vehicleAssignment) {
            VehicleAssignment = vehicleAssignment;
        }


        // Setter Methods


    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "GeneralCoverage",
            "VehicleCoverage",
            "StateSpecificCoverage"
    })
    public static class Coverages {
        GeneralCoverage GeneralCoverage = new GeneralCoverage();

        public ArrayList<VehicleCoverage> getVehicleCoverage() {
            return VehicleCoverage;
        }

        public void setVehicleCoverage(ArrayList<VehicleCoverage> vehicleCoverages) {
            VehicleCoverage = vehicleCoverages;
        }

        ArrayList<VehicleCoverage> VehicleCoverage = new ArrayList<VehicleCoverage>();
        StateSpecificCoverage StateSpecificCoverage = new StateSpecificCoverage();


        // Getter Methods

        public GeneralCoverage getGeneralCoverage() {
            return GeneralCoverage;
        }

        public StateSpecificCoverage getStateSpecificCoverage() {
            return StateSpecificCoverage;
        }

        // Setter Methods

        public void setGeneralCoverage(GeneralCoverage GeneralCoverage) {
            this.GeneralCoverage = GeneralCoverage;
        }

        public void setStateSpecificCoverage(StateSpecificCoverage StateSpecificCoverage) {
            this.StateSpecificCoverage = StateSpecificCoverage;
        }
    }

    public static class StateSpecificCoverage {
        public IMAutoData.DECoverages getDECoverages() {
            return DECoverages;
        }

        public void setDECoverages(IMAutoData.DECoverages DECoverages) {
            this.DECoverages = DECoverages;
        }

        DECoverages DECoverages = new DECoverages();



    }

    //    @XmlAccessorType(XmlAccessType.FIELD)
////    @XmlType(name = "", propOrder = {
////            "DEPIP",
////            "DEPIPDeductible",
////            "DEPIPAppliesTo",
////            "DEAPIP"
////    })
    public static class DECoverages

    {
//        private String DEPIP;
//        private String DEPIPDeductible;
//        private String DEPIPAppliesTo;
//        private String DEAPIP;

//        String name;
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getValue() {
//            return value;
//        }
//
//        public void setValue(String value) {
//            this.value = value;
//        }
//
//        String value;


        public void setcoverages(Map<String, String> coverages) {
            this.coverages = coverages;
        }

        private Map<String, String> coverages = new HashMap<String, String>();

        @XmlAnyElement
        public List<JAXBElement<String>> getcoverages() {
            List<JAXBElement<String>> elements = new ArrayList<JAXBElement<String>>();
            for (Map.Entry<String, String> coverage: coverages.entrySet())
                elements.add(new JAXBElement(new QName(coverage.getKey()),
                        String.class, coverage.getValue()));
            return elements;
        }


// Getter Methods

//        public String getDEPIP() {
//            return DEPIP;
//        }
//
//        public String getDEPIPDeductible() {
//            return DEPIPDeductible;
//        }
//
//        public String getDEPIPAppliesTo() {
//            return DEPIPAppliesTo;
//        }
//
//        public String getDEAPIP() {
//            return DEAPIP;
//        }
//
//// Setter Methods
//
//        public void setDEPIP(String DEPIP) {
//            this.DEPIP = DEPIP;
//        }
//
//        public void setDEPIPDeductible(String DEPIPDeductible) {
//            this.DEPIPDeductible = DEPIPDeductible;
//        }
//
//        public void setDEPIPAppliesTo(String DEPIPAppliesTo) {
//            this.DEPIPAppliesTo = DEPIPAppliesTo;
//        }
//
//        public void setDEAPIP(String DEAPIP) {
//            this.DEAPIP = DEAPIP;
//        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "BI",
            "PD",
            "MP",
            "UM",
            "UIM",
            "UIUIM",
            "AAADiscount",
            "Multicar"
    })
    public static class GeneralCoverage {
        private String BI;
        private String PD;
        private String MP;
        private String UM;
        private String UIM;
        private String UIUIM;
        private String AAADiscount;
        private String Multicar;


        // Getter Methods

        public String getBI() {
            return BI;
        }

        public String getPD() {
            return PD;
        }

        public String getMP() {
            return MP;
        }

        public String getUM() {
            return UM;
        }

        public String getUIM() {
            return UIM;
        }
        
         public String getUIUIM() {
            return UIUIM;
        }

        public String getAAADiscount() {
            return AAADiscount;
        }

        public String getMulticar() {
            return Multicar;
        }

        // Setter Methods

        public void setBI(String BI) {
            this.BI = BI;
        }

        public void setPD(String PD) {
            this.PD = PD;
        }

        public void setMP(String MP) {
            this.MP = MP;
        }

        public void setUM(String UM) {
            this.UM = UM;
        }
        public void setUIUIM(String UIUIM) {
            this.UIUIM = UIUIM;
        }

        public void setUIM(String UIM) {
            this.UIM = UIM;
        }

        public void setAAADiscount(String AAADiscount) {
            this.AAADiscount = AAADiscount;
        }

        public void setMulticar(String Multicar) {
            this.Multicar = Multicar;
        }
    }

    public static class VehiclesUse {
        ArrayList<VehicleUse> VehicleUse = new ArrayList<VehicleUse>();


        // Getter Methods

        public ArrayList<com.unirateservices.universal_rate.VehicleUse> getVehicleUse() {
            return VehicleUse;
        }

        public void setVehicleUse(ArrayList<com.unirateservices.universal_rate.VehicleUse> vehicleUse) {
            VehicleUse = vehicleUse;
        }


        // Setter Methods


    }

    public static class Vehicles {
        ArrayList<Vehicle> Vehicle = new ArrayList<Vehicle>();

        public ArrayList<Vehicle> getVehicle() {
            return Vehicle;
        }

        public void setVehicle(ArrayList<Vehicle> vehiclesObject) {
            Vehicle = vehiclesObject;
        }


//        public ArrayList<Vehicle> getVehicle() {
//            return vehicle;
//        }
//
//        public void setVehicle(ArrayList<Vehicle> vehicle) {
//            vehicle = vehicle;
//        }
        // Getter Methods


        // Setter Methods

    }
//



    public static class Drivers {
        ArrayList<Driver> Driver = new ArrayList<Driver>();

        public ArrayList<Driver> getDriver() {
            return Driver;
        }

        public void setDriver(ArrayList<Driver> driver) {
            Driver = driver;
        }
        // Getter Methods


        // Setter Methods


    }

    public static class ResidenceInfo {
        CurrentAddress CurrentAddress = new CurrentAddress();


        // Getter Methods

        public CurrentAddress getCurrentAddress() {
            return CurrentAddress;
        }

        // Setter Methods

        public void setCurrentAddress(CurrentAddress CurrentAddress) {
            this.CurrentAddress = CurrentAddress;
        }
    }

    public static class CurrentAddress {
        YearsAtCurrent YearsAtCurrent = new YearsAtCurrent();
        private String Ownership;


        // Getter Methods

        public YearsAtCurrent getYearsAtCurrent() {
            return YearsAtCurrent;
        }

        public String getOwnership() {
            return Ownership;
        }

        // Setter Methods

        public void setYearsAtCurrent(YearsAtCurrent YearsAtCurrentObject) {
            this.YearsAtCurrent = YearsAtCurrentObject;
        }

        public void setOwnership(String Ownership) {
            this.Ownership = Ownership;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "Years",
            "Months"
    })
    public static class YearsAtCurrent {
        private Integer Years;
        private Integer Months;


        // Getter Methods

        public Integer getYears() {
            return Years;
        }

        public Integer getMonths() {
            return Months;
        }

        // Setter Methods

        public void setYears(Integer Years) {
            this.Years = Years;
        }

        public void setMonths(Integer Months) {
            this.Months = Months;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "PolicyTerm",
            "Package",
            "Effective",
            "CreditCheckAuth"
    })
    public static class PolicyInfo {
        private String PolicyTerm;
        private String Package;
        private String Effective;
        private String CreditCheckAuth;


        // Getter Methods

        public String getPolicyTerm() {
            return PolicyTerm;
        }

        public String getPackage() {
            return Package;
        }

        public String getEffective() {
            return Effective;
        }

        public String getCreditCheckAuth() {
            return CreditCheckAuth;
        }

        // Setter Methods

        public void setPolicyTerm(String PolicyTerm) {
            this.PolicyTerm = PolicyTerm;
        }

        public void setPackage(String Package) {
            this.Package = Package;
        }

        public void setEffective(String Effective) {
            this.Effective = Effective;
        }

        public void setCreditCheckAuth(String CreditCheckAuth) {
            this.CreditCheckAuth = CreditCheckAuth;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "PriorCarrier",
            "ReasonNoPrior",
            "Expiration",
            "YearsWithPriorCarrier",
            "YearsWithContinuousCoverage",
            "PriorLiabilityLimit",
            "ReasonForLapse",
            "PriorPolicyPremium",
            "PriorPolicyTerm"
    })
    public static class PriorPolicyInfo {
        private String PriorCarrier;
        private String ReasonNoPrior;
        private String Expiration;
          private String ReasonForLapse;
        YearsWithPriorCarrier YearsWithPriorCarrier = new YearsWithPriorCarrier();
        YearsWithContinuousCoverage YearsWithContinuousCoverage = new YearsWithContinuousCoverage();
        private String PriorLiabilityLimit;
        private Integer PriorPolicyPremium;
        private String PriorPolicyTerm;


        // Getter Methods

        public String getPriorCarrier() {
            return PriorCarrier;
        }

        public String getExpiration() {
            return Expiration;
        }

        public String getReasonNoPrior() {
            return  ReasonNoPrior;
        }
                public String getReasonForLapse() {
            return  ReasonForLapse;
        }




        public YearsWithPriorCarrier getYearsWithPriorCarrier() {
            return YearsWithPriorCarrier;
        }

        public YearsWithContinuousCoverage getYearsWithContinuousCoverage() {
            return YearsWithContinuousCoverage;
        }

        public String getPriorLiabilityLimit() {
            return PriorLiabilityLimit;
        }

        public Integer getPriorPolicyPremium() {
            return PriorPolicyPremium;
        }

        public String getPriorPolicyTerm() {
            return PriorPolicyTerm;
        }

        // Setter Methods

        public void setPriorCarrier(String PriorCarrier) {
            this.PriorCarrier = PriorCarrier;
        }

        public void setExpiration(String Expiration) {
            this.Expiration = Expiration;
        }
        
        public void setReasonNoPrior(String ReasonNoPrior) {
            this.ReasonNoPrior = ReasonNoPrior;
        }
         public void setReasonForLapse(String ReasonForLapse) {
            this.ReasonForLapse = ReasonForLapse;
        }

        public void setYearsWithPriorCarrier(YearsWithPriorCarrier YearsWithPriorCarrierObject) {
            this.YearsWithPriorCarrier= YearsWithPriorCarrierObject;
        }

        public void setYearsWithContinuousCoverage(YearsWithContinuousCoverage YearsWithContinuousCoverage) {
            this.YearsWithContinuousCoverage = YearsWithContinuousCoverage;
        }

        public void setPriorLiabilityLimit(String PriorLiabilityLimit) {
            this.PriorLiabilityLimit = PriorLiabilityLimit;
        }

        public void setPriorPolicyPremium(Integer PriorPolicyPremium) {
            this.PriorPolicyPremium = PriorPolicyPremium;
        }

        public void setPriorPolicyTerm(String PriorPolicyTerm) {
            this.PriorPolicyTerm = PriorPolicyTerm;
        }
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "Years",
                "Months"
        })
        public static class YearsWithContinuousCoverage {
            private String Years;
            private String Months;


            // Getter Methods

            public String getYears() {
                return Years;
            }

            public String getMonths() {
                return Months;
            }

            // Setter Methods

            public void setYears(String Years) {
                this.Years = Years;
            }

            public void setMonths(String Months) {
                this.Months = Months;
            }
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "Years",
                "Months"
        })
        public static class YearsWithPriorCarrier {
            private String Years;
            private String Months;


            // Getter Methods

            public String getYears() {
                return Years;
            }

            public String getMonths() {
                return Months;
            }

            // Setter Methods

            public void setYears(String Years) {
                this.Years = Years;
            }

            public void setMonths(String Months) {
                this.Months = Months;
            }
        }
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ApplicantType",
            "PersonalInfo",
            "Address"
    })
    public static class Applicant {
        private String ApplicantType;

        PersonalInfo PersonalInfo = new PersonalInfo();
        Address Address = new Address();


        public Applicant(){

        }
        // Getter Methods

        public String getApplicantType() {
            return ApplicantType;
        }

        public PersonalInfo getPersonalInfo() {
            return PersonalInfo;
        }

        public Address getAddress() {
            return Address;
        }

        // Setter Methods

        public void setApplicantType(String ApplicantType) {
            this.ApplicantType = ApplicantType;
        }

        public void setPersonalInfo(PersonalInfo PersonalInfo) {
            this.PersonalInfo = PersonalInfo;
        }

        public void setAddress(Address Address) {
            this.Address = Address;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "AddressCode",
            "Addr1",
            "City",
            "StateCode",
            "County",
            "Zip5",
            "Zip4",
            "Phone",
            "Email",
            "Validation"
    })
    public static class Address {
        private String AddressCode;
        Addr1 Addr1 = new Addr1();
        private String City;
        private String StateCode;
        private String County;
        private String Zip5;
        private String Zip4;

        public Phone getPhone() {
            return Phone;
        }

        public void setPhone(Phone phone) {
            Phone = phone;
        }

        Phone Phone = new Phone();
        private String Email;
        private String Validation;


        // Getter Methods

        public String getAddressCode() {
            return AddressCode;
        }

        public Addr1 getAddr1() {
            return Addr1;
        }

        public String getCity() {
            return City;
        }

        public String getStateCode() {
            return StateCode;
        }

        public String getCounty() {
            return County;
        }

        public String getZip5() {
            return Zip5;
        }

        public String getZip4() {
            return Zip4;
        }

        public String getEmail() {
            return Email;
        }

        public String getValidation() {
            return Validation;
        }

        // Setter Methods

        public void setAddressCode(String AddressCode) {
            this.AddressCode = AddressCode;
        }

        public void setAddr1(Addr1 Addr1) {
            this.Addr1 = Addr1;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public void setStateCode(String StateCode) {
            this.StateCode = StateCode;
        }

        public void setCounty(String County) {
            this.County = County;
        }

        public void setZip5(String Zip5) {
            this.Zip5 = Zip5;
        }

        public void setZip4(String Zip4) {
            this.Zip4 = Zip4;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public void setValidation(String Validation) {
            this.Validation = Validation;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "StreetName",
                "StreetNumber"
        })
        public static class Addr1 {
            private String StreetName;
            private String StreetNumber;


            // Getter Methods

            public String getStreetName() {
                return StreetName;
            }

            public String getStreetNumber() {
                return StreetNumber;
            }

            // Setter Methods

            public void setStreetName(String StreetName) {
                this.StreetName = StreetName;
            }

            public void setStreetNumber(String StreetNumber) {
                this.StreetNumber = StreetNumber;

            }

        }
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "PhoneType",
                "PhoneNumberType",
                "PhoneNumber"
        })
        public static  class Phone{
            String PhoneType;

            public String getPhoneNumberType() {
                return PhoneNumberType;
            }

            public void setPhoneNumberType(String phoneNumberType) {
                PhoneNumberType = phoneNumberType;
            }

            public String getPhoneType() {
                return PhoneType;
            }

            public void setPhoneType(String phoneType) {
                PhoneType = phoneType;
            }

            public String getPhoneNumber() {
                return PhoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                PhoneNumber = phoneNumber;
            }

            String PhoneNumberType;
            String PhoneNumber;
            public Phone(){

            }

        }
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "Name",
            "DOB",
            "Gender",
            "MaritalStatus",
            "Industry",
            "Occupation",
            "Education",
            "Relation"
    })
    public static class PersonalInfo {

        Name Name = new Name();
        private String DOB;
        private String Gender;
        private String MaritalStatus;
        private String Industry;
        private String Occupation;
        private String Education;
        private String Relation;


        // Getter Methods

        public Name getName() {
            return Name;
        }

        public String getDOB() {
            return DOB;
        }

        public String getGender() {
            return Gender;
        }

        public String getMaritalStatus() {
            return MaritalStatus;
        }

        public String getIndustry() {
            return Industry;
        }

        public String getOccupation() {
            return Occupation;
        }

        public String getEducation() {
            return Education;
        }

        public String getRelation() {
            return Relation;
        }

        // Setter Methods

        public void setName(Name Name) {
            this.Name = Name;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public void setGender(String Gender) {
            this.Gender = Gender;
        }

        public void setMaritalStatus(String MaritalStatus) {
            this.MaritalStatus = MaritalStatus;
        }

        public void setIndustry(String Industry) {
            this.Industry = Industry;
        }

        public void setOccupation(String Occupation) {
            this.Occupation = Occupation;
        }

        public void setEducation(String Education) {
            this.Education = Education;
        }

        public void setRelation(String Relation) {
            this.Relation = Relation;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "FirstName",
                "LastName"
        })
        public static class Name {
            private String FirstName;
            private String LastName;


            // Getter Methods

            public String getFirstName() {
                return FirstName;
            }

            public String getLastName() {
                return LastName;
            }

            // Setter Methods

            public void setFirstName(String FirstName) {
                this.FirstName = FirstName;
            }

            public void setLastName(String LastName) {
                this.LastName = LastName;
            }
        }
    }
}