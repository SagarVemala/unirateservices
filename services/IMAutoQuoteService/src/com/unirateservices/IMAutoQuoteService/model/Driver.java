/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "Name",
        "Gender",
        "DOB",
        "DLNumber",
        "DLState",
        "DateLicensed",
        "DLStatus",
        "AgeLicensed",
        "MaritalStatus",
        "Relation",
        "Industry",
        "Occupation",
        "GoodStudent",
        "Student100",
        "GoodDriver",
        "DriverTraining",
        "MATDriver",
        "PrincipalVehicle",
        "Accident",
        "Violation",
        "SR22",
        "LicenseRevokedSuspended"
})
@XmlRootElement(name = "Driver")
public class Driver {
    private String GoodStudent;

    private ArrayList<Violation> Violation;

    private String LicenseRevokedSuspended;

    private Integer PrincipalVehicle;

    private String DriverTraining;

    private String DLStatus;

    private String MATDriver;

    private String DateLicensed;

    private String Gender;

    
    private Name Name;

    private String Industry;

    private String MaritalStatus;

    private String Occupation;

    private String Student100;

    private String Relation;

    private ArrayList<Accident> Accident;

    private String DOB;

    private String DLNumber;

    private String DLState;

    private String SR22;

    private Integer AgeLicensed;
    
    private String GoodDriver;

   @XmlAttribute(name="ID")
    private Integer Id;
    

    public String getGoodStudent ()
    {
        return GoodStudent;
    }

    public void setGoodStudent (String GoodStudent)
    {
        this.GoodStudent = GoodStudent;
    }

    public ArrayList<Violation> getViolation ()
    {
        return Violation;
    }

    public void setViolation (ArrayList<Violation> Violation)
    {
        this.Violation = Violation;
    }

    public String getLicenseRevokedSuspended ()
    {
        return LicenseRevokedSuspended;
    }

    public void setLicenseRevokedSuspended (String LicenseRevokedSuspended)
    {
        this.LicenseRevokedSuspended = LicenseRevokedSuspended;
    }

    public Integer getPrincipalVehicle ()
    {
        return PrincipalVehicle;
    }

    public void setPrincipalVehicle (Integer PrincipalVehicle)
    {
        this.PrincipalVehicle = PrincipalVehicle;
    }

    public String getDriverTraining ()
    {
        return DriverTraining;
    }

    public void setDriverTraining (String DriverTraining)
    {
        this.DriverTraining = DriverTraining;
    }

    public String getDLStatus ()
    {
        return DLStatus;
    }

    public void setDLStatus (String DLStatus)
    {
        this.DLStatus = DLStatus;
    }

    public String getMATDriver ()
    {
        return MATDriver;
    }

    public void setMATDriver (String MATDriver)
    {
        this.MATDriver = MATDriver;
    }
    
    
       public String getGoodDriver ()
    {
        return GoodDriver;
    }

    public void setGoodDriver (String GoodDriver)
    {
        this.GoodDriver = GoodDriver;
    }

    public String getDateLicensed ()
    {
        return DateLicensed;
    }

    public void setDateLicensed (String DateLicensed)
    {
        this.DateLicensed = DateLicensed;
    }

    public String getGender ()
    {
        return Gender;
    }

    public void setGender (String Gender)
    {
        this.Gender = Gender;
    }

    public Name getName ()
    {
        return Name;
    }

    public void setName (Name Name)
    {
        this.Name = Name;
    }

    public String getIndustry ()
    {
        return Industry;
    }

    public void setIndustry (String Industry)
    {
        this.Industry = Industry;
    }

    public String getMaritalStatus ()
    {
        return MaritalStatus;
    }

    public void setMaritalStatus (String MaritalStatus)
    {
        this.MaritalStatus = MaritalStatus;
    }

    public String getOccupation ()
    {
        return Occupation;
    }

    public void setOccupation (String Occupation)
    {
        this.Occupation = Occupation;
    }

    public String getStudent100 ()
    {
        return Student100;
    }

    public void setStudent100 (String Student100)
    {
        this.Student100 = Student100;
    }

    public String getRelation ()
    {
        return Relation;
    }

    public void setRelation (String Relation)
    {
        this.Relation = Relation;
    }

    public ArrayList<Accident> getAccident ()
    {
        return Accident;
    }

    public void setAccident (ArrayList<Accident> Accident)
    {
        this.Accident = Accident;
    }

    public String getDOB ()
    {
        return DOB;
    }

    public void setDOB (String DOB)
    {
        this.DOB = DOB;
    }

    public String getDLNumber ()
    {
        return DLNumber;
    }

    public void setDLNumber (String DLNumber)
    {
        this.DLNumber = DLNumber;
    }

    public String getDLState ()
    {
        return DLState;
    }

    public void setDLState (String DLState)
    {
        this.DLState = DLState;
    }

    public String getSR22 ()
    {
        return SR22;
    }

    public void setSR22 (String SR22)
    {
        this.SR22 = SR22;
    }

    public Integer getAgeLicensed ()
    {
        return AgeLicensed;
    }

    public void setAgeLicensed (Integer AgeLicensed)
    {
        this.AgeLicensed = AgeLicensed;
    }

    public Integer getId ()
    {
        return Id;
    }

    public void setId (Integer Id)
    {
        this.Id = Id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [GoodStudent = "+GoodStudent+", Violation = "+Violation+", LicenseRevokedSuspended = "+LicenseRevokedSuspended+", PrincipalVehicle = "+PrincipalVehicle+", DriverTraining = "+DriverTraining+", DLStatus = "+DLStatus+", MATDriver = "+MATDriver+", DateLicensed = "+DateLicensed+", Gender = "+Gender+", Name = "+Name+", Industry = "+Industry+", MaritalStatus = "+MaritalStatus+", Occupation = "+Occupation+", Student100 = "+Student100+", Relation = "+Relation+", Accident = "+Accident+", DOB = "+DOB+", DLNumber = "+DLNumber+", DLState = "+DLState+", SR22 = "+SR22+", AgeLicensed = "+AgeLicensed+", Id = "+Id+"]";
    }
}