/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.*;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "Useage",
        "OneWayMiles",
        "DaysPerWeek",
        "WeeksPerMonth",
        "AnnualMiles",
        "Ownership",
        "Carpool",
        "AlternateGarage",
        "PrincipalOperator",
        "UsedForDelivery",
        "PriorDamagePresent"
})
public class VehicleUse {
    private Integer PrincipalOperator;

    private String Ownership;

    private String Carpool;

    private String AlternateGarage;

    private Integer DaysPerWeek;

    private String PriorDamagePresent;

    private String Useage;

    private Integer WeeksPerMonth;

    private Integer OneWayMiles;

    private Integer AnnualMiles;
    
@XmlAttribute(name="ID")
    private Integer Id;

    private String UsedForDelivery;

    public Integer getPrincipalOperator ()
    {
        return PrincipalOperator;
    }

    public void setPrincipalOperator (Integer PrincipalOperator)
    {
        this.PrincipalOperator = PrincipalOperator;
    }

    public String getOwnership ()
    {
        return Ownership;
    }

    public void setOwnership (String Ownership)
    {
        this.Ownership = Ownership;
    }

    public String getCarpool ()
    {
        return Carpool;
    }

    public void setCarpool (String Carpool)
    {
        this.Carpool = Carpool;
    }

    public String getAlternateGarage ()
    {
        return AlternateGarage;
    }

    public void setAlternateGarage (String AlternateGarage)
    {
        this.AlternateGarage = AlternateGarage;
    }

    public Integer getDaysPerWeek ()
    {
        return DaysPerWeek;
    }

    public void setDaysPerWeek (Integer DaysPerWeek)
    {
        this.DaysPerWeek = DaysPerWeek;
    }

    public String getPriorDamagePresent ()
    {
        return PriorDamagePresent;
    }

    public void setPriorDamagePresent (String PriorDamagePresent)
    {
        this.PriorDamagePresent = PriorDamagePresent;
    }

    public String getUseage ()
    {
        return Useage;
    }

    public void setUseage (String Useage)
    {
        this.Useage = Useage;
    }

    public Integer getWeeksPerMonth ()
    {
        return WeeksPerMonth;
    }

    public void setWeeksPerMonth (Integer WeeksPerMonth)
    {
        this.WeeksPerMonth = WeeksPerMonth;
    }

    public Integer getOneWayMiles ()
    {
        return OneWayMiles;
    }

    public void setOneWayMiles (Integer OneWayMiles)
    {
        this.OneWayMiles = OneWayMiles;
    }

    public Integer getAnnualMiles ()
    {
        return AnnualMiles;
    }

    public void setAnnualMiles (Integer AnnualMiles)
    {
        this.AnnualMiles = AnnualMiles;
    }

    public Integer getId ()
    {
        return Id;
    }

    public void setId (Integer Id)
    {
        this.Id = Id;
    }

    public String getUsedForDelivery ()
    {
        return UsedForDelivery;
    }

    public void setUsedForDelivery (String UsedForDelivery)
    {
        this.UsedForDelivery = UsedForDelivery;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PrincipalOperator = "+PrincipalOperator+", Ownership = "+Ownership+", Carpool = "+Carpool+", AlternateGarage = "+AlternateGarage+", DaysPerWeek = "+DaysPerWeek+", PriorDamagePresent = "+PriorDamagePresent+", Useage = "+Useage+", WeeksPerMonth = "+WeeksPerMonth+", OneWayMiles = "+OneWayMiles+", AnnualMiles = "+AnnualMiles+", Id = "+Id+", UsedForDelivery = "+UsedForDelivery+"]";
    }
}