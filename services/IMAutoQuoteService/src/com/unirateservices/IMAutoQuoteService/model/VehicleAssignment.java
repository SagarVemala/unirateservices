/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class VehicleAssignment {
    
    
    private DriverAssignment DriverAssignment;

     @XmlAttribute(name="Id")
    private Integer Id;

    public DriverAssignment getDriverAssignment ()
    {
        return DriverAssignment;
    }

    public void setDriverAssignment (DriverAssignment DriverAssignment)
    {
        this.DriverAssignment = DriverAssignment;
    }

    public Integer getId ()
    {
        return Id;
    }

    public void setId (Integer Id)
    {
        this.Id = Id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DriverAssignment = "+DriverAssignment+", Id = "+Id+"]";
    }
}