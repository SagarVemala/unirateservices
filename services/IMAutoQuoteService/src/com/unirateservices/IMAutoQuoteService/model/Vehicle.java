/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "UseVinLookup",
        "UseVehicleLookupService",
        "Year",
        "Vin",
        "Make",
        "Model",
        "SubModel",
        "AntiTheft",
        "PassiveRestraints",
        "AntiLockBrake"
})
public class Vehicle {

    public Vehicle(){

    }

        private String Year;

        private String UseVehicleLookupService;

        private String UseVinLookup;

        private String Model;

        private String PassiveRestraints;

        private String Vin;

        private String Make;

        private String AntiTheft;

        private String AntiLockBrake;
        
        @XmlAttribute(name="Id")
        private Integer Id;

        @XmlElement(name="Sub-Model")
        private String SubModel;

        public String getYear() {
            return Year;
        }

        public void setYear(String Year) {
            this.Year = Year;
        }

        public String getUseVehicleLookupService() {
            return UseVehicleLookupService;
        }

        public void setUseVehicleLookupService(String UseVehicleLookupService) {
            this.UseVehicleLookupService = UseVehicleLookupService;
        }

        public String getUseVinLookup() {
            return UseVinLookup;
        }

        public void setUseVinLookup(String UseVinLookup) {
            this.UseVinLookup = UseVinLookup;
        }

        public String getModel() {
            return Model;
        }

        public void setModel(String Model) {
            this.Model = Model;
        }

        public String getPassiveRestraints() {
            return PassiveRestraints;
        }

        public void setPassiveRestraints(String PassiveRestraints) {
            this.PassiveRestraints = PassiveRestraints;
        }

        public String getVin() {
            return Vin;
        }

        public void setVin(String Vin) {
            this.Vin = Vin;
        }

        public String getMake() {
            return Make;
        }

        public void setMake(String Make) {
            this.Make = Make;
        }

        public String getAntiTheft() {
            return AntiTheft;
        }

        public void setAntiTheft(String AntiTheft) {
            this.AntiTheft = AntiTheft;
        }

        public String getAntiLockBrake() {
            return AntiLockBrake;
        }

        public void setAntiLockBrake(String AntiLockBrake) {
            this.AntiLockBrake = AntiLockBrake;
        }

        public Integer getId() {
            return Id;
        }

        public void setId(Integer Id) {
            this.Id = Id;
        }

        public String getSubModel() {
            return SubModel;
        }

        public void setSubModel(String SubModel) {
            this.SubModel = SubModel;
        }

        @Override
        public String toString() {
            return "ClassPojo [Year = " + Year + ", UseVehicleLookupService = " + UseVehicleLookupService + ", UseVinLookup = " + UseVinLookup + ", Model = " + Model + ", PassiveRestraints = " + PassiveRestraints + ", Vin = " + Vin + ", Make = " + Make + ", Anti-Theft = " + AntiTheft + ", AntiLockBrake = " + AntiLockBrake + ", Id = " + Id + ", Sub-Model = " + SubModel + "]";
        }
    }