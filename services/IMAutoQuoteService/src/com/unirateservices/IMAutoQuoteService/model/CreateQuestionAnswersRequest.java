/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;



import java.util.List;

public class CreateQuestionAnswersRequest {


    public ImQuestionMaster getQuestion() {
        return question;
    }

    public void setQuestion(ImQuestionMaster question) {
        this.question = question;
    }

    public List<ImQuestionAnswerLookup> getAnswers() {
        return answers;
    }

    public void setAnswers(List<ImQuestionAnswerLookup> answers) {
        this.answers = answers;
    }

    ImQuestionMaster question;
    List<ImQuestionAnswerLookup> answers;



}