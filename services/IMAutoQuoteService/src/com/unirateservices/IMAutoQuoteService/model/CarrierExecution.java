/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


// @XmlRootElement(name ="CarrierExecution")
// @XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="CarrierExecution")
public class CarrierExecution {
    private Integer CarrierID;

     @XmlElement(name = "PropertyValues", required = true)
    private PropertyValues PropertyValues= new PropertyValues();

    public Integer getCarrierID ()
    {
        return CarrierID;
    }

    public void setCarrierID (Integer CarrierID)
    {
        this.CarrierID = CarrierID;
    }

    public PropertyValues getPropertyValues ()
    {
        return PropertyValues;
    }

    public void setPropertyValues (PropertyValues PropertyValues)
    {
        this.PropertyValues = PropertyValues;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CarrierID = "+CarrierID+", PropertyValues = "+PropertyValues+"]";
    }
}