/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.universal_rate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "Date",
        "Description",
        "PD",
        "BI",
        "VehicleInvolved"
})
public class Accident {

    private String Description;

    private Integer PD;

    private Integer BI;

    @XmlAttribute(name="name")
    private Integer Id;

    private Integer VehicleInvolved;

    private String Date;

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public Integer getPD ()
    {
        return PD;
    }

    public void setPD (Integer PD)
    {
        this.PD = PD;
    }

    public Integer getBI ()
    {
        return BI;
    }

    public void setBI (Integer BI)
    {
        this.BI = BI;
    }

    public Integer getId ()
    {
        return Id;
    }

    public void setId (Integer Id)
    {
        this.Id = Id;
    }

    public Integer getVehicleInvolved ()
    {
        return VehicleInvolved;
    }

    public void setVehicleInvolved (Integer VehicleInvolved)
    {
        this.VehicleInvolved = VehicleInvolved;
    }

    public String getDate ()
    {
        return Date;
    }

    public void setDate (String Date)
    {
        this.Date = Date;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description = "+Description+", PD = "+PD+", BI = "+BI+", Id = "+Id+", VehicleInvolved = "+VehicleInvolved+", Date = "+Date+"]";
    }
}