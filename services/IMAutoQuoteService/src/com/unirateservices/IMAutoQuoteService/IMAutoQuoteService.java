
/*Copyright (c) 2016-2017 imaginea.com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea.com*/
package com.unirateservices.IMAutoQuoteService;

import com.unirateservices.DriverAssignment;
import com.unirateservices.VehicleAssignment;
import com.unirateservices.universal_rate.*;
import com.unirateservices.universal_rate.models.query.GetCarrierStatePolicyQuestionsResponse;
import com.unirateservices.universal_rate.models.query.GetCarrierSpecificQuestionsResponse;
import com.unirateservices.universal_rate.service.*;
import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.io.*;

import org.json.*;
import org.json.XML;

import com.unirateservices.carrier.Carrier;

//import com.unirateservices.IMAutoQuoteService.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class IMAutoQuoteService {



    private static final Logger logger = LoggerFactory.getLogger(IMAutoQuoteService.class);
    
    @Value("${app.environment.ratingServiceUrl}")
    private String ratingServiceUrl;
    
    @Autowired
    private SecurityService securityService;

    @Autowired
    ImApplicantService imApplicantService;

    @Autowired
    ImAddressService addressService;

    @Autowired
    ImPolicyInfoService policyInfoService;

    @Autowired
    ImPriorPolicyInfoService  priorPolicyInfoService;

    @Autowired
    ImResidenceInfoService imResidenceInfoService;

    @Autowired
    ImDriverService driverService;

    @Autowired
    ImVehicleService vehicleService;

    @Autowired
    ImVehicleUseService vehicleUseService;

    @Autowired
    ImVehicleAssignmentService vehicleAssignmentService;

    @Autowired
    ImCoverageService coverageService;

    @Autowired
    ImIncidentService incidentService;


    @Autowired
    ImVehicleCoverageService imVehicleCoverageService;

    @Autowired
    ImStateSpecificCoverageService imStateSpecificCoverageService;

    @Autowired
    ImCarrierExclusionsService carrierExclusionsService;

    @Autowired
    UNIVERSAL_RATEQueryExecutorService queryService;



    IMAutoData imAutoData = new IMAutoData();
    static String stateCode ="";
    static int numDrivers;
    List<ImDriver> drivers = new ArrayList<ImDriver>();

   

    public ArrayList<Carrier> buildIMAutoRequest(Integer policyId, List<Integer> carrierIds){

        String query = "policyId =" + policyId;
        imAutoData.setPolicyId(policyId);

        Page<ImApplicant> applicantPage = imApplicantService.findAll(query, null);
        if(applicantPage.getContent().size()>0){
            ImApplicant applicant =applicantPage.getContent().get(0);



            //Pageable pageable =  new PageRequest(0, 10);
            Page<ImAddress> addresses = addressService.findAll(query, null);

            if(addresses.getContent().size()>0){
                ImAddress imAddress = addresses.getContent().get(0);
                setApplicantData(applicant, imAddress);
            }


        }



        Page<ImPolicyInfo> policyInfoPage = policyInfoService.findAll(query, null);
        if(policyInfoPage.getContent().size()>0){
            ImPolicyInfo policyInfo =  policyInfoPage.getContent().get(0);
            setPolicyInfo(policyInfo);
        }


        Page<ImPriorPolicyInfo> priorPolicyInfoPage = priorPolicyInfoService.findAll(query, null);
        if(priorPolicyInfoPage.getContent().size()>0){
            ImPriorPolicyInfo priorPolicyInfo= priorPolicyInfoPage.getContent().get(0);
            setPriorPolicyInfo(priorPolicyInfo);
        }


        Page<ImResidenceInfo> imResidenceInfoPage =imResidenceInfoService.findAll(query,null);
        // if(imResidenceInfoPage.getContent().size()>0){
        //     ImResidenceInfo residenceInfo =  imResidenceInfoPage.getContent().get(0);
            setResidenceInfo(null);
        // }else{
        //     logger.info("resInfo has no values");
        // }


        Page<ImDriver> driversPage = driverService.findAll(query, null);
        if(driversPage.getContent().size()>0){
            drivers = driverService.findAll(query, null).getContent();
            setDrivers(drivers,policyId);
            numDrivers=drivers.size();
        }


        Page<ImVehicle> vehcilesPage  =  vehicleService.findAll(query,null);
        if(vehcilesPage.getContent().size()>0){
            List<ImVehicle> vehicles =vehicleService.findAll(query,null).getContent();
            setVehicles(vehicles);

        }


        Page<ImVehicleUse> vehcilesUsePage   =vehicleUseService.findAll(query, null);
        if(vehcilesUsePage.getContent().size()>0){
            List<ImVehicleUse> vehicleUses = vehcilesUsePage.getContent();
            setVehicleUses(vehicleUses);
        }

        Page<ImVehicleAssignment> vehicleAssignmentspage =vehicleAssignmentService.findAll(query, null);
        if(vehicleAssignmentspage.getContent().size()>0){
            List<ImVehicleAssignment> vehicleAssignments =vehicleAssignmentspage.getContent();
            setVehicleAssignments(vehicleAssignments);
        }else{
            logger.info("count for vehassign is zero");
        }





//        if(coveragesPage.getContent().size()>0){
        // ImCoverage imCoverage = coveragesPage.getContent().get(0);
        setCoverages(policyId);
        //}
        logger.info("carrierIds length" + carrierIds);
        String getStateName = imAutoData.getApplicant().getAddress().getStateCode()!=null? imAutoData.getApplicant().getAddress().getStateCode():"";
        if(carrierIds != null && carrierIds.size() > 0){
            Pageable pageable =  new PageRequest(0,Integer.MAX_VALUE);
            Page<GetCarrierSpecificQuestionsResponse> propertyValuesPage = queryService.executeGetCarrierSpecificQuestions(getStateName,carrierIds, policyId, pageable);
                    logger.info("specific carriers..." + propertyValuesPage.getContent());


        if(propertyValuesPage.getContent().size()>0){
            List<GetCarrierSpecificQuestionsResponse> propertyValues = propertyValuesPage.getContent();
            setSpecificCarriers(propertyValues);
        }
        }
        else{
            Page<GetCarrierStatePolicyQuestionsResponse> propertyValuesPage = queryService.executeGetCarrierStatePolicyQuestions(getStateName, policyId, new PageRequest(0,Integer.MAX_VALUE));
    logger.info("All carriers..." + propertyValuesPage.getContent());
            if(propertyValuesPage.getContent().size()>0){
                List<GetCarrierStatePolicyQuestionsResponse> propertyValues = propertyValuesPage.getContent();
    
                setCarriersForPolicy(propertyValues);
            }            
        }

        String result="";



        try {
            result =generateXmlForAutoData();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

       // logger.info("generated xml" +result);
           String stateCovName =stateCode + "-" + "Coverages";
            result =result.replaceAll("DECoverages",stateCovName);
            logger.info("the state code is set to" +stateCovName);

        result=result.replaceAll("<firstName","<FirstName");
        result=result.replaceAll("driver","Driver");
        result=result.replaceAll("ownership","Ownership");
        result=result.replaceAll("value name","value Name");
        result=result.replaceAll("carrierID","CarrierID");


        result=result.replaceAll("vehicle","Vehicle");



        result=result.replace("yearsAtCurrent","YearsAtCurrent");

        result=result.replace("currentAddress","CurrentAddress");


        result=result.replace("vehicleAssignment","VehicleAssignment");


        result=result.replace("ratingStateCode","RatingStateCode");

//

        result=result.replace("firstName","FirstName");
        result=result.replace("lastName","LastName");
        result=result.replace("id=","ID=");
            result=result.replaceAll("Id=","id=");
            result=result.replaceAll("ID=","id=");
            
         
        StringBuilder sb = new StringBuilder(result);

        while(sb.indexOf("<propertyValues>")!=-1) {

            int index1 = sb.indexOf("<propertyValues>");
            int index2 = sb.indexOf("</propertyValues>") + 18;


            sb.replace(index1, index2, "");
           
        }
        while(sb.indexOf("<carrierExecution>")!=-1) {

            int index1 = sb.indexOf("<carrierExecution>");
            int index2 = sb.indexOf("</carrierExecution>") + 20;


            sb.replace(index1, index2, "");

        }
        
        while(sb.indexOf("{Driver_index}")!=-1){
            int count=1;
            int index1 = sb.indexOf("{Driver_index}");
            int index2 = index1 + 14;
            sb.replace(index1, index2, String.valueOf(count));
            count++;

        }
        
         while(sb.indexOf("{Driver_Index}")!=-1){
            int count=1;
            int index1 = sb.indexOf("{Driver_Index}");
            int index2 = index1 + 14;
            sb.replace(index1, index2, String.valueOf(count));
            count++;

        }


        while(sb.indexOf("{Vehicle_index}")!=-1){
            int count=1;
            int index1 = sb.indexOf("{Vehicle_index}");
            int index2 = index1 + 15;
            sb.replace(index1, index2, String.valueOf(count));
            count++;

        }
        
         while(sb.indexOf("{Vehicle_Index}")!=-1){
            int count=1;
            int index1 = sb.indexOf("{Vehicle_Index}");
            int index2 = index1 + 15;
            sb.replace(index1, index2, String.valueOf(count));
            count++;

        }

       logger.info(sb.toString());
        ArrayList<Carrier> premiumData = restService(sb.toString());
               

        
        return premiumData;

    }

    private void setSpecificCarriers(List<GetCarrierSpecificQuestionsResponse> propertyValuesIn) {

        ArrayList<CarrierExecution>  carrierExecutionList = new ArrayList<>();
        Map<Integer, ArrayList<PropertyValue>> carrierMap = new HashMap<>();
        for(int i=0 ; i<propertyValuesIn.size();i++ ) {

            GetCarrierSpecificQuestionsResponse propertyValueIn = propertyValuesIn.get(i);

            PropertyValue propertyValue = new PropertyValue();

                propertyValue.setContent(propertyValueIn.getAnswerText());
            
            logger.info("content is  "+ propertyValueIn.getQuestionText());
                       propertyValue.setPropertyName(propertyValueIn.getNodeName());

            int carrierId = propertyValueIn.getCarrierId();

            if (!carrierMap.containsKey(carrierId)) {
                ArrayList<PropertyValue> tempList = new ArrayList<>();
                tempList.add(propertyValue);
                carrierMap.put(carrierId, tempList);

            } else {
                ArrayList<PropertyValue> tempList = carrierMap.get(carrierId);
                tempList.add(propertyValue);
                carrierMap.put(carrierId, tempList);
            }
        }
        logger.info("size of the mao is "+ carrierMap.size());
        for(Integer carriedId : carrierMap.keySet()){
            CarrierExecution carrierExecution1 = new CarrierExecution();
            carrierExecution1.setCarrierID(carriedId);
            PropertyValues values = new PropertyValues();
            values.setValue(carrierMap.get(carriedId));
            carrierExecution1.setPropertyValues(values);
            carrierExecutionList.add(carrierExecution1);

        }

        imAutoData.getCarriers().setCarrierExecution(carrierExecutionList);

    }



    private void setCarriersForPolicy(List<GetCarrierStatePolicyQuestionsResponse> propertyValuesIn) {

        ArrayList<CarrierExecution>  carrierExecutionList = new ArrayList<>();
        Map<Integer, ArrayList<PropertyValue>> carrierMap = new HashMap<>();
        for(int i=0 ; i<propertyValuesIn.size();i++ ) {

            GetCarrierStatePolicyQuestionsResponse propertyValueIn = propertyValuesIn.get(i);

            PropertyValue propertyValue = new PropertyValue();

                propertyValue.setContent(propertyValueIn.getAnswerText());
            
            logger.info("content is  "+ propertyValueIn.getQuestionText());
                       propertyValue.setPropertyName(propertyValueIn.getNodeName().toLowerCase());

            int carrierId = propertyValueIn.getCarrierId();

            if (!carrierMap.containsKey(carrierId)) {
                ArrayList<PropertyValue> tempList = new ArrayList<>();
                tempList.add(propertyValue);
                carrierMap.put(carrierId, tempList);

            } else {
                ArrayList<PropertyValue> tempList = carrierMap.get(carrierId);
                tempList.add(propertyValue);
                carrierMap.put(carrierId, tempList);
            }
        }
        logger.info("size of the mao is "+ carrierMap.size());
        for(Integer carriedId : carrierMap.keySet()){
            CarrierExecution carrierExecution1 = new CarrierExecution();
            carrierExecution1.setCarrierID(carriedId);
            PropertyValues values = new PropertyValues();
            values.setValue(carrierMap.get(carriedId));
            carrierExecution1.setPropertyValues(values);
            carrierExecutionList.add(carrierExecution1);

        }

        imAutoData.getCarriers().setCarrierExecution(carrierExecutionList);







    }

    private void setCoverages(Integer policyId) {
        String query = "policyId =" + policyId;

        Page<ImCoverage> coveragesPage = coverageService.findAll(query,null);


        if(coveragesPage.getContent().size()>0) {
            ImCoverage incoverage = coveragesPage.getContent().get(0);
            logger.info("in set Coverages");


            IMAutoData.Coverages coverages = new IMAutoData().getCoverages();

            IMAutoData.GeneralCoverage generalCoverage = new IMAutoData().getCoverages().getGeneralCoverage();


            generalCoverage.setBI(incoverage.getBi() != null ? incoverage.getBi() : "");
            generalCoverage.setAAADiscount("No");
            
               String stateName = imAutoData.getApplicant().getAddress().getStateCode()!=null? imAutoData.getApplicant().getAddress().getStateCode():"";
               
            if(incoverage.getMp() != null){
                generalCoverage.setMP(incoverage.getMp());    
            }else{
                if(stateName.equalsIgnoreCase("DC") || stateName.equalsIgnoreCase("DE")  || stateName.equalsIgnoreCase("MI")  || stateName.equalsIgnoreCase("DC")  || stateName.equalsIgnoreCase("PA")  ){
                    generalCoverage.setMP("None");    
                }
                
            }
            generalCoverage.setMulticar("No");
            generalCoverage.setPD(incoverage.getPd() != null ? incoverage.getPd() : "");
            
            if(incoverage.getUm() != null){
                generalCoverage.setUM(incoverage.getUm());    
            }else{
                 if(stateName.equalsIgnoreCase("CA")  || stateName.equalsIgnoreCase("FL")  || stateName.equalsIgnoreCase("MD")){
                 generalCoverage.setUM("None");   
                 }
            }
            if(incoverage.getUim() != null){
                generalCoverage.setUIM(incoverage.getUim());    
            }else{
                if(stateName.equalsIgnoreCase("CA")   || stateName.equalsIgnoreCase("FL")   || stateName.equalsIgnoreCase("MD")  ){
                generalCoverage.setUIM("None");  
                }
            }
             if(incoverage.getUiuim() != null){
                generalCoverage.setUIUIM(incoverage.getUiuim());    
            }
            coverages.setGeneralCoverage(generalCoverage);


            imAutoData.setCoverages(coverages);
        }


        Page<ImStateSpecificCoverage> inStateSpecificCoveragePage =imStateSpecificCoverageService.findAll(query,null);
        Map<String,String> coveragesMap = new LinkedHashMap<>();

        logger.info("outside the state specific coverage code polc id seleetced is"+policyId);
        if(inStateSpecificCoveragePage.getContent().size()>0){
            logger.info("inside the state specific coverage code key to");
            List<ImStateSpecificCoverage> imStateSpecificCoverageS =inStateSpecificCoveragePage.getContent();
            IMAutoData.StateSpecificCoverage outStateSpecificCoverage = new IMAutoData().getCoverages().getStateSpecificCoverage();

            IMAutoData.DECoverages deCoverages = new IMAutoData.DECoverages();

            for(ImStateSpecificCoverage stateCoverage: imStateSpecificCoverageS){
                String covName =stateCoverage.getCoverageName();
                //covName = stateCode + "-" + covName;
                covName= covName.replaceAll("\\s+",""); 
                covName= covName.replaceAll("\\/","");
                covName = covName.replaceAll("Type","");
                coveragesMap.put(covName,stateCoverage.getCoverageValue());
            }
            deCoverages.setcoverages(coveragesMap);

            outStateSpecificCoverage.setDECoverages(deCoverages);

            imAutoData.getCoverages().setStateSpecificCoverage(outStateSpecificCoverage);

        }

        Page<ImVehicleCoverage> vehicleCoveragePage =imVehicleCoverageService.findAll(query,null);


        if(vehicleCoveragePage.getContent().size()>0){
            VehicleCoverage vehicleCoverage ;
            List<ImVehicleCoverage> imVehicleCoverages = vehicleCoveragePage.getContent();
                int count=1;
            for(ImVehicleCoverage imVehicleCoverage :imVehicleCoverages){
                
                vehicleCoverage = new VehicleCoverage();

                vehicleCoverage.setId(count);
                
                count=count+1;
                vehicleCoverage.setCollisionDeductible(imVehicleCoverage.getCollDeduct()!=null?imVehicleCoverage.getCollDeduct():"");
                vehicleCoverage.setFullGlass(imVehicleCoverage.getFullGlass()!=null?imVehicleCoverage.getFullGlass():"No");
                vehicleCoverage.setLiabilityNotRequired("No");
                vehicleCoverage.setLoanLeaseCoverage(imVehicleCoverage.getLoanLeseCov()!=null?imVehicleCoverage.getLoanLeseCov():"No");
                if(imVehicleCoverage.getComprehensive()!=null){
                    vehicleCoverage.setOtherCollisionDeductible(imVehicleCoverage.getComprehensive());    
                }
                
                vehicleCoverage.setRentalDeductible(imVehicleCoverage.getRentalDeduct()!=null?imVehicleCoverage.getRentalDeduct():"");
                vehicleCoverage.setTowingDeductible(imVehicleCoverage.getTowingDeduct()!=null?imVehicleCoverage.getTowingDeduct():"");
                vehicleCoverage.setStatedAmount(imVehicleCoverage.getStatedAmount());
                vehicleCoverage.setReplacementCost("No");
                imAutoData.getCoverages().getVehicleCoverage().add(vehicleCoverage);
            }
        }






    }

    private String generateXmlForAutoData() throws JAXBException, FileNotFoundException {

        JAXBContext contextObj = JAXBContext.newInstance(IMAutoData.class);

        Marshaller marshallerObj = contextObj.createMarshaller();
        // marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        // marshallerObj.marshal(imAutoData, new FileOutputStream("imAutoData.xml"));
        // File f = new File("imAutoData.xml");
        marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        StringWriter sw = new StringWriter();
        marshallerObj.marshal(imAutoData, sw);

        String result = sw.toString();
        //logger.info("converting response to xml" +result);
        return result;
    }

private void setVehicleAssignments(List<ImVehicleAssignment> inVehicleAssignments) {
   
   // if(inVehicleAssignments.size()==1 || inVehicleAssignments.size()!=numDrivers){
    if(numDrivers==1){
          ArrayList<VehicleAssignment> outVehicleAssignemt = new ArrayList<VehicleAssignment>();
      VehicleAssignment vehAssigment;
      DriverAssignment driverAssignment;
      int count=1;

      for(ImVehicleAssignment inVehAssignment: inVehicleAssignments){

          vehAssigment = new VehicleAssignment();
          driverAssignment = new DriverAssignment();
          logger.info("count for vehAss"+count);
          vehAssigment.setId(count);
          driverAssignment.setId(1);
          count=count+1;

          driverAssignment.setContent("100");
          vehAssigment.setDriverAssignment(driverAssignment);
          outVehicleAssignemt.add(vehAssigment);
      }
      imAutoData.getVehicleAssignments().setVehicleAssignment(outVehicleAssignemt);
        
    }//else  if(inVehicleAssignments.size()==2 && numDrivers==2){
    else  if(numDrivers==2){
        driverTwo(inVehicleAssignments.size());    
    }//else if(inVehicleAssignments.size()==3){
    else if(numDrivers==3){
         driverThree(inVehicleAssignments.size());    
    } else if(numDrivers==4){
         driverFour(inVehicleAssignments.size());    
    } else if(numDrivers==5){
         driverFive(inVehicleAssignments.size());    
    } else if(numDrivers==6){
         driverSix(inVehicleAssignments.size());    
    }

    

   }
    private void setVehicleUses(List<ImVehicleUse> inVehicleUses) {

        IMAutoData.VehiclesUse vehicleUses = new IMAutoData().getVehiclesUse();
        ArrayList<VehicleUse> outVehiclesUse = new ArrayList<VehicleUse>();
        int count=1;
        for(ImVehicleUse inVehicleUse :inVehicleUses){
            VehicleUse vehUse = new VehicleUse();
            

            vehUse.setId(count);
            count=count+1;
            logger.info("counter for driverAssignment"+count);
            vehUse.setUseage(inVehicleUse.getUseage()!=null?inVehicleUse.getUseage():"");
            if(inVehicleUse.getOneWayMiles()!=null){
                vehUse.setOneWayMiles(inVehicleUse.getOneWayMiles());    
            }else{
                 vehUse.setOneWayMiles(0);    
            }
            
            vehUse.setDaysPerWeek(inVehicleUse.getDaysPerWk()!=null?inVehicleUse.getDaysPerWk():0);
            vehUse.setWeeksPerMonth(inVehicleUse.getWeeksPerMnth()!=null?inVehicleUse.getWeeksPerMnth():0);
            vehUse.setAnnualMiles(inVehicleUse.getAnnualMiles()!=null?inVehicleUse.getAnnualMiles():0);
            //vehUse.setOwnership(inVehicleUse.getOwnership()!=null?inVehicleUse.getOwnership():"Ownership");
            vehUse.setOwnership("Owned");
            vehUse.setCarpool("No");
            vehUse.setAlternateGarage("No");
            vehUse.setPrincipalOperator(1);
            vehUse.setUsedForDelivery("No");
            vehUse.setPriorDamagePresent("No");
            outVehiclesUse.add(vehUse);

        }

        imAutoData.getVehiclesUse().setVehicleUse(outVehiclesUse);

    }

    private void setVehicles(List<ImVehicle> inVehicles) {

        IMAutoData.Vehicles vehicles = new IMAutoData().getVehicle();
        ArrayList<Vehicle> outVehicles = new ArrayList<Vehicle>();

        int count=1;
        for(ImVehicle invehicle:inVehicles){
            Vehicle vehicle = new Vehicle();
            
            vehicle.setId(count);
            logger.info("counter for vehicle"+count);
            count=count+1;
            vehicle.setAntiTheft(invehicle.getAntiTheft()!=null?invehicle.getAntiTheft():"");
            vehicle.setMake(invehicle.getMake()!=null?invehicle.getMake():"");
            vehicle.setAntiLockBrake(invehicle.getAntiLockBrake()!=null&&invehicle.getAntiLockBrake().length()>0 ?invehicle.getAntiLockBrake():"No");
            vehicle.setModel(invehicle.getModel()!=null?invehicle.getModel():"");
            vehicle.setUseVinLookup("No");
            vehicle.setPassiveRestraints("None");
            vehicle.setSubModel(invehicle.getSubModel()!=null?invehicle.getSubModel():"");
            vehicle.setUseVehicleLookupService("No");
            vehicle.setYear(invehicle.getYear()!=null?invehicle.getYear():"");
            vehicle.setVin(invehicle.getVin()!=null?invehicle.getVin():"");
            outVehicles.add(vehicle);
        }

        imAutoData.getVehicle().setVehicle(outVehicles);

    }

    private void setDrivers(List<ImDriver> inDrivers,Integer policyId) {

        IMAutoData.Drivers drivers = new IMAutoData().getDrivers();

        ArrayList<Driver> ourDrivers = new ArrayList<Driver>();
        int count=1;
        for(ImDriver inDriver : inDrivers){

            Driver driver = new Driver();
            
            driver.setGender(inDriver.getGender()!=null?inDriver.getGender():"");
            driver.setId(count);
            logger.info("count for driver"+count);
            count=count+1;
            driver.setDOB(inDriver.getDob()!=null?changeDateFormat1(inDriver.getDob()):"");
            driver.setDLNumber(inDriver.getDriverLicenseNumber()!=null?inDriver.getDriverLicenseNumber():"");
             String getStateName = imAutoData.getApplicant().getAddress().getStateCode()!=null? imAutoData.getApplicant().getAddress().getStateCode():"";
            driver.setDLState(getStateName);
            driver.setDateLicensed(inDriver.getDateLicensed()!=null?changeDateFormat1(inDriver.getDateLicensed()):"");
            driver.setDLStatus(inDriver.getDriverLicenseStatus()!=null?inDriver.getDriverLicenseStatus():"");
            driver.setAgeLicensed(inDriver.getAgeLicensed()!=null?inDriver.getAgeLicensed():0);
            driver.setMaritalStatus(inDriver.getMaritalStatus()!=null?inDriver.getMaritalStatus():"");
            driver.setRelation(inDriver.getRelation()!=null?inDriver.getRelation():"");
            driver.setIndustry(inDriver.getIndustry()!=null?inDriver.getIndustry():"");
            driver.setOccupation(inDriver.getOccupation()!=null?inDriver.getOccupation():"");
         // driver.setStudent100(inDriver.getStudent100()!=null?inDriver.getStudent100():"Yes");
         logger.info("inDriver.getGoodStudent()"+inDriver.getGoodStudent());
         logger.info("inDriver.getStudent100()"+inDriver.getStudent100());
         logger.info("inDriver.getDriverTraining()"+inDriver.getDriverTraining());
         logger.info("inDriver.getMatDriver()"+inDriver.getMatDriver());
          if(inDriver.getGoodStudent().indexOf("'")==-1 && inDriver.getGoodStudent()!=null ){
            driver.setGoodStudent(inDriver.getGoodStudent());  
          }
            if(inDriver.getStudent100().indexOf("'")==-1 && inDriver.getStudent100()!=null ){
            driver.setStudent100(inDriver.getStudent100());  
          }
            if(inDriver.getDriverTraining().indexOf("'")==-1 &&  inDriver.getDriverTraining()!=null ){
            driver.setDriverTraining(inDriver.getDriverTraining());  
          }
     
            if(inDriver.getMatDriver().indexOf("'")==-1 && inDriver.getMatDriver()!=null ){
            driver.setMATDriver(inDriver.getMatDriver());  
          }
              if(inDriver.getGoodDriver().indexOf("'")==-1 && inDriver.getGoodDriver()!=null ){
            driver.setGoodDriver(inDriver.getGoodDriver());  
            
          }
            
          //  driver.setDriverTraining(inDriver.getDriverTraining()!=null?inDriver.getDriverTraining():"Yes");
            //driver.setMATDriver(inDriver.getMatDriver());
            driver.setPrincipalVehicle(inDriver.getPrincipalVehicle()!=null?inDriver.getPrincipalVehicle():1);
            //driver.setLicenseRevokedSuspended(inDriver.getLicenseRevokedSuspended()!=null?inDriver.getLicenseRevokedSuspended():"No");
           // driver.setSR22(inDriver.getSr22()!=null?inDriver.getSr22():"Yes");
           driver.setSR22("No");
           driver.setLicenseRevokedSuspended("No");
            Name name = new Name();
            name.setFirstName(inDriver.getFirstName()!=null?inDriver.getFirstName():"");
            name.setLastName(inDriver.getLastName()!=null?inDriver.getLastName():"");
            driver.setName(name);
               
            Calendar cal = Calendar.getInstance();
            DateFormat dateFormatNeeded = new SimpleDateFormat("yyyy-MM-dd");
            
               try {
                cal.setTime(dateFormatNeeded.parse(driver.getDOB()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            
            cal.add(Calendar.YEAR, inDriver.getAgeLicensed());
            
            driver.setDateLicensed(dateFormatNeeded.format(cal.getTime()));

            ArrayList<Accident> accidents = new ArrayList<Accident>();
            String query = "driverId =" + inDriver.getDriverId() + " and " + " INCIDENT_TYPE='Accident'";
            Page<ImIncident> accidentsPage = incidentService.findAll(query, null);
            if(accidentsPage.getContent().size()>0){
                List<ImIncident> inAccidents = accidentsPage.getContent();
                int count1=1;
                logger.info("found accidents size"+inAccidents.size());
                for(ImIncident inAccident:inAccidents){
                    
                    Accident accident = new Accident();
                    accident.setId(count1);
                    count1++;
                    accident.setDate(inAccident.getDate()!=null?changeDateFormat1(inAccident.getDate()):"");
                    //   priorPolicyInfo.setExpiration(inputPriorPolicyInfo.getExpiration()!=null?changeDateFormat1(inputPriorPolicyInfo.getExpiration()):"");
                    
                    accident.setDescription(inAccident.getDescription()!=null?inAccident.getDescription():"");
                    accident.setPD(inAccident.getPd()!=null?inAccident.getPd():0);
                    accident.setBI(inAccident.getBi()!=null?inAccident.getBi():0);
                    accident.setVehicleInvolved(inAccident.getVehicleInvolved());
                    accidents.add(accident);
                }


            }
            driver.setAccident(accidents);



            ArrayList<Violation> violations = new ArrayList<Violation>();
            query = "driverId =" + inDriver.getDriverId()  + " and " + "INCIDENT_TYPE='Violation'";
            Page<ImIncident> violationsPage = incidentService.findAll(query, null);
            if(violationsPage.getContent().size()>0){
                List<ImIncident> inVoilations = violationsPage.getContent();
                logger.info("found violations size"+inVoilations.size());
                int count2=1;
                for(ImIncident inVoilation:inVoilations){
                    Violation violation = new Violation();
                    
                    violation.setId(count2);
                    count2++;
                    violation.setDate(inVoilation.getDate() != null ? changeDateFormat1(inVoilation.getDate()) : "");
                    if(inVoilation.getDescription() != null){
                        String violationDesc = inVoilation.getDescription();
                        if(inVoilation.getDescription().equals("Speeding 1-14")){
                            violationDesc = "Speeding 6-10";
                        }
                        else if(inVoilation.getDescription().equals("Speeding 15-20")){
                            violationDesc = "Speeding 16-20";
                        }
                        violation.setDescription(violationDesc);
                    }
                    else{
                        violation.setDescription("");
                    }
                    
                    violations.add(violation);
                }


            }
            driver.setViolation(violations);
            ourDrivers.add(driver);

        }
        drivers.setDriver(ourDrivers);
        imAutoData.setDrivers(drivers);





    }

    private void setResidenceInfo(ImResidenceInfo inputResidenceInfo) {
        logger.info("Inside residence info");

        IMAutoData.ResidenceInfo residenceInfo = imAutoData.getResidenceInfo();

        IMAutoData.CurrentAddress currentAddress = new IMAutoData.CurrentAddress();

        currentAddress.setOwnership("Rental Home/Condo");
        IMAutoData.YearsAtCurrent yearsAtCurrent= imAutoData.getResidenceInfo().getCurrentAddress().getYearsAtCurrent();
        yearsAtCurrent.setYears(1);
        yearsAtCurrent.setMonths(0);
        currentAddress.setYearsAtCurrent(yearsAtCurrent);
        residenceInfo.setCurrentAddress(currentAddress);

        imAutoData.setResidenceInfo(residenceInfo);


//        address.set
    }

    private void setPriorPolicyInfo(ImPriorPolicyInfo inputPriorPolicyInfo) {
        IMAutoData.PriorPolicyInfo priorPolicyInfo = imAutoData.getPriorPolicyInfo();
        // priorPolicyInfo.setExpiration(inputPriorPolicyInfo.getExpiration()!=null?inputPriorPolicyInfo.getExpiration():"");
        // priorPolicyInfo.setPriorCarrier(inputPriorPolicyInfo.getPriorC());
  
        
        // Date date = new Date();
        // String todaysDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        // priorPolicyInfo.setExpiration(todaysDate);
        
       // priorPolicyInfo.setExpiration("2019-04-01");
  
        priorPolicyInfo.setPriorCarrier(inputPriorPolicyInfo.getPriorCarrierType()!=null?inputPriorPolicyInfo.getPriorCarrierType():"");

         if(inputPriorPolicyInfo.getReasonNoPrior()!=null){
            priorPolicyInfo.setReasonNoPrior(inputPriorPolicyInfo.getReasonNoPrior());
        }else{
                  priorPolicyInfo.setExpiration(inputPriorPolicyInfo.getExpiration()!=null?changeDateFormat1(inputPriorPolicyInfo.getExpiration()):"");
                      priorPolicyInfo.setPriorLiabilityLimit(inputPriorPolicyInfo.getPriorLiabilityLimit()!=null?inputPriorPolicyInfo.getPriorLiabilityLimit():"");
        if(inputPriorPolicyInfo.getPriorPolicyPremium()!=null){
        priorPolicyInfo.setPriorPolicyPremium(inputPriorPolicyInfo.getPriorPolicyPremium());    
        }
        if(inputPriorPolicyInfo.getReasonForLapse()!=null){
            priorPolicyInfo.setReasonForLapse(inputPriorPolicyInfo.getReasonForLapse());
        }
                  priorPolicyInfo.setPriorPolicyTerm("6 Month");
        
        IMAutoData.PriorPolicyInfo.YearsWithContinuousCoverage yearsWithContinuousCoverage =imAutoData.getPriorPolicyInfo().getYearsWithContinuousCoverage();
        yearsWithContinuousCoverage.setMonths(inputPriorPolicyInfo.getMonthsWithContinuousCoverage()!=null?inputPriorPolicyInfo.getMonthsWithContinuousCoverage():"0");
        yearsWithContinuousCoverage.setYears(inputPriorPolicyInfo.getYearsWithContinuousCoverage()!=null?inputPriorPolicyInfo.getYearsWithContinuousCoverage():"0");
        priorPolicyInfo.setYearsWithContinuousCoverage(yearsWithContinuousCoverage);



        IMAutoData.PriorPolicyInfo.YearsWithPriorCarrier yearsWithPriorCarrier= imAutoData.getPriorPolicyInfo().getYearsWithPriorCarrier();
        yearsWithPriorCarrier.setYears(inputPriorPolicyInfo.getYearsWithPriorCarrier()!=null?inputPriorPolicyInfo.getYearsWithPriorCarrier():"1");
        yearsWithPriorCarrier.setMonths(inputPriorPolicyInfo.getMonthsWithPriorCarrier()!=null?inputPriorPolicyInfo.getMonthsWithPriorCarrier():"0");
        priorPolicyInfo.setYearsWithPriorCarrier(yearsWithPriorCarrier);
        }
        
        
        
  
        imAutoData.setPriorPolicyInfo(priorPolicyInfo);
    }

    private void setPolicyInfo(ImPolicyInfo inputPolicyInfo) {
        IMAutoData.PolicyInfo polInfo = new IMAutoData().getPolicyInfo();
        polInfo.setCreditCheckAuth("Yes");
        polInfo.setEffective(inputPolicyInfo.getEffective() != null ? changeDateFormat1(inputPolicyInfo.getEffective()) : "");
        // polInfo.setEffective("2019-04-01");
        polInfo.setPackage("No");
        polInfo.setPolicyTerm("12 Month");
        imAutoData.setPolicyInfo(polInfo);


    }

    private void setApplicantData(ImApplicant applicant, ImAddress imaddress) {

        IMAutoData.Applicant app = new IMAutoData().getApplicant();

        //applicantType
        app.setApplicantType("Applicant");


        //personalInfo
        IMAutoData.PersonalInfo personalInfo = new IMAutoData().getApplicant().getPersonalInfo();
        personalInfo.setDOB(applicant.getDob()!=null?changeDateFormat1(applicant.getDob()):"");
        personalInfo.setEducation(applicant.getEducation()!=null?applicant.getEducation():"");
        personalInfo.setGender(applicant.getGender()!=null?applicant.getGender():"");
        personalInfo.setIndustry(applicant.getIndustry()!=null?applicant.getIndustry():"");
        personalInfo.setMaritalStatus(applicant.getMaritalStatus()!=null?applicant.getMaritalStatus():null);
        personalInfo.setOccupation(applicant.getOccupation()!=null?applicant.getOccupation():"");
        
        

        IMAutoData.PersonalInfo.Name name = new IMAutoData().getApplicant().getPersonalInfo().getName();
        name.setFirstName(applicant.getFirstName()!=null?applicant.getFirstName():"");
        name.setLastName(applicant.getLastName()!=null?applicant.getLastName():"");

        personalInfo.setName(name);
        app.setPersonalInfo(personalInfo);

        //Address

        IMAutoData.Address address = new IMAutoData().getApplicant().getAddress();

        address.setCity(imaddress.getCity()!=null?imaddress.getCity():"");
        address.setAddressCode("StreetAddress");
        address.setCounty(imaddress.getCounty()!=null?imaddress.getCounty():"");
        address.setStateCode(imaddress.getStateCode()!=null?imaddress.getStateCode():"");
        if(imaddress.getEmail()!=null){
            address.setEmail(imaddress.getEmail());    
        }
        
        address.setValidation(imaddress.getValidation()!=null?imaddress.getValidation():"");
        address.setZip4(imaddress.getZipCode4()!=null?imaddress.getZipCode4():"");
        address.setZip5(imaddress.getZipCode5()!=null?imaddress.getZipCode5():"");

        address.setValidation("Valid");


        IMAutoData.Address.Addr1 addr1 =new IMAutoData().getApplicant().getAddress().getAddr1();

        String streetName = imaddress.getAddress1()!=null?imaddress.getAddress1():"";
        //String streetName = imaddress.getAddress2()!=null?imaddress.getAddress2().replace("[0-9]",""):"";
        
        if(streetName.length()>0){
            int in= streetName.indexOf(" "); 
            
            if(in==-1){
                 addr1.setStreetName(streetName);
                  addr1.setStreetNumber("");
                
            }else{
            addr1.setStreetNumber(streetName.substring(0,in));
           addr1.setStreetName(streetName.substring(in+1,streetName.length()));
            }
          
        }else{
    addr1.setStreetName("");
        addr1.setStreetNumber("");
            
        }
        




 
        address.setAddr1(addr1);



        IMAutoData.Address.Phone phoneObject = new IMAutoData().getApplicant().getAddress().getPhone();

        phoneObject.setPhoneType("Home");
        phoneObject.setPhoneNumberType("Phone");
        String phoneNumber = imaddress.getPhoneNumber();
        if(phoneNumber!=null){
        
        phoneNumber=phoneNumber.replaceAll("[()]", "");
        phoneNumber= phoneNumber.replaceAll("-","");
        phoneNumber= phoneNumber.replaceAll("\\s+","");     
        }
       
        phoneObject.setPhoneNumber(phoneNumber!=null?phoneNumber:"");


        //List<IMAutoData.Address.PhoneObject> phones = new ArrayList<>();

        //phone.add(phoneObject);

        address.setPhone(phoneObject);


        app.setAddress(address);

        //GeneralInfo

        IMAutoData.GeneralInfo generalInfo = new IMAutoData().getGeneralInfo();

        generalInfo.setRatingStateCode(address.getStateCode()!=null?address.getStateCode():"");
        stateCode = address.getStateCode()!=null?address.getStateCode():"";


        imAutoData.setGeneralInfo(generalInfo);
        imAutoData.setApplicant(app);



    }

    private ImAddress getAddress(){

        return null;
    }

    private ImVehicle getVehicle(Integer policyId){

        ImVehicleServiceImpl service = new ImVehicleServiceImpl();
        Pageable pageable =  new PageRequest(0, 1);

        Page<ImVehicle> list = service.findAll("policy_id= "+policyId, pageable);
        return list.getContent().get(0);
    }


    private ImApplicant getApplicant(Integer policyId){
        //ImApplicantServiceImpl service = new ImApplicantServiceImpl();
//        QueryFilter filter = new QueryFilter("policyId",policyId,Type.EQUALS,AttributeType.INTEGER);
//        List<QueryFilter> filters = new ArrayList<QueryFilter>();
        //filters.add(filter);
        Pageable pageable =  new PageRequest(0, 10);
        String query = "policyId =" + policyId;

        Page<ImApplicant> list = imApplicantService.findAll(query, null);
        logger.info("applicant name"+list.getContent().get(0).getFirstName());
        logger.info("applicant last name"+list.getContent().get(0).getLastName());
        logger.info("applicant last name"+list.getContent().get(0).getDob());
        logger.info("applicant last name"+list.getContent().get(0).toString());
        return list.getContent().get(0);
    }
    
        private String changeDateFormat(String dateReceivedFromUser) {

        DateFormat userDateFormat = new SimpleDateFormat("MM-dd-yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("yyyy-MM-dd");
         dateFormatNeeded.setLenient(true);
        Date date = null;
        try {
            date = userDateFormat.parse(dateReceivedFromUser);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String convertedDate = dateFormatNeeded.format(date);
        System.out.print(convertedDate);
        return convertedDate;


    }

    private String changeDateFormat1(String dateReceivedFromUser) {
        DateFormat userDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("yyyy-MM-dd");
        
        dateFormatNeeded.setLenient(true);
        Date date = null;
        try {
            date = userDateFormat.parse(dateReceivedFromUser);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(date != null){
           String convertedDate = dateFormatNeeded.format(date);
        System.out.print(convertedDate);
        return convertedDate; 
        }
        
        return null;
        
            }
            
    private void driverOne(int vehicleCount){
        
        ArrayList<VehicleAssignment> outVehicleAssignemt = new ArrayList<VehicleAssignment>();
        VehicleAssignment vehAssigment;
        DriverAssignment driverAssignment;
        int count=1;
        
    	if(vehicleCount == 1){
    	    vehAssigment = new VehicleAssignment();
            driverAssignment = new DriverAssignment();
            logger.info("count for vehAss"+count);
            vehAssigment.setId(count);
            driverAssignment.setId(1);
            count=count+1;
            driverAssignment.setContent("100");
            vehAssigment.setDriverAssignment(driverAssignment);
            outVehicleAssignemt.add(vehAssigment);
            imAutoData.getVehicleAssignments().setVehicleAssignment(outVehicleAssignemt);
    	}
    	else if(vehicleCount == 2){
    	  for(int i=0; i< vehicleCount; i++){
    	    vehAssigment = new VehicleAssignment();
            driverAssignment = new DriverAssignment();
            logger.info("count for vehAss"+count);
            vehAssigment.setId(count);
            driverAssignment.setId(1);
            if(count == 1){
               driverAssignment.setContent("51"); 
            }
            else {
                driverAssignment.setContent("49");
            }
            
            vehAssigment.setDriverAssignment(driverAssignment);
            outVehicleAssignemt.add(vehAssigment);
            imAutoData.getVehicleAssignments().setVehicleAssignment(outVehicleAssignemt);
            count=count+1;

    	}
    	}
    	else if(vehicleCount == 3){
    	    
    	}
    	else if(vehicleCount == 4){
    	    
    	}
    	
    }
    private void driverTwo(int vehicleCount){
        
        String[][] myArray = {{"51","49"}, {"49","51"}, {"51","49",},{"49","51"}};
        ArrayList<VehicleAssignment> outVehicleAssignemt = new ArrayList<VehicleAssignment>();
        VehicleAssignment vehAssigment;
            DriverAssignment driverAssignment;
        for(int i = 0; i < vehicleCount; i++){
            logger.info("inside the driverTwo logic"+ i);
            //outVehicleAssignemt = new ArrayList<VehicleAssignment>();
            for(int j = 0; j < 2; j++){
            vehAssigment = new VehicleAssignment();
            driverAssignment = new DriverAssignment();
            vehAssigment.setId(i + 1);
            driverAssignment.setId(j + 1);
            driverAssignment.setContent(myArray[i][j]);
            vehAssigment.setDriverAssignment(driverAssignment);
            outVehicleAssignemt.add(vehAssigment);
           
            }
        }

    	 imAutoData.getVehicleAssignments().setVehicleAssignment(outVehicleAssignemt);
    	 logger.info("size of veh assignment is *********"+ outVehicleAssignemt.size());
    }
    private void driverThree(int vehicleCount){
        
         String[][] myArray = {{"34","33","33"}, {"33","34","33"}, {"33","33","34"},{"34","33","33"}};
         ArrayList<VehicleAssignment> outVehicleAssignemt = new ArrayList<VehicleAssignment>();
        VehicleAssignment vehAssigment;
            DriverAssignment driverAssignment;
          for(int i = 0; i < vehicleCount; i++){
            logger.info("inside the driverThree logic"+ i);
            //outVehicleAssignemt = new ArrayList<VehicleAssignment>();
            for(int j = 0; j < 3; j++){
            vehAssigment = new VehicleAssignment();
            driverAssignment = new DriverAssignment();
            vehAssigment.setId(i + 1);
            driverAssignment.setId(j + 1);
            driverAssignment.setContent(myArray[i][j]);
            vehAssigment.setDriverAssignment(driverAssignment);
            outVehicleAssignemt.add(vehAssigment);
           
            }
        }
         imAutoData.getVehicleAssignments().setVehicleAssignment(outVehicleAssignemt);
    	 logger.info("size of veh assignment is *********"+ outVehicleAssignemt.size());
    }
    private void driverFour(int vehicleCount){
        
        String [][] myArray = {{"26","24","25","25"}, {"24","26","25","25"}, {"25","25","26","24"},{"25","25","24","26"}};
          ArrayList<VehicleAssignment> outVehicleAssignemt = new ArrayList<VehicleAssignment>();
        VehicleAssignment vehAssigment;
            DriverAssignment driverAssignment;
          for(int i = 0; i < vehicleCount; i++){
            logger.info("inside the driverThree logic"+ i);
            //outVehicleAssignemt = new ArrayList<VehicleAssignment>();
            for(int j = 0; j < 4; j++){
            vehAssigment = new VehicleAssignment();
            driverAssignment = new DriverAssignment();
            vehAssigment.setId(i + 1);
            driverAssignment.setId(j + 1);
            driverAssignment.setContent(myArray[i][j]);
            vehAssigment.setDriverAssignment(driverAssignment);
            outVehicleAssignemt.add(vehAssigment);
           
            }
        }
         imAutoData.getVehicleAssignments().setVehicleAssignment(outVehicleAssignemt);
    	 logger.info("size of veh assignment is *********"+ outVehicleAssignemt.size());
    
    }
    private void driverFive(int vehicleCount){
         String[][] myArray = {{"21","19","20","20","20"}, {"19","21","20","20","20"}, {"20","20","21","19","20"},{"20","20","19","21","20"}};
      ArrayList<VehicleAssignment> outVehicleAssignemt = new ArrayList<VehicleAssignment>();
        VehicleAssignment vehAssigment;
            DriverAssignment driverAssignment;
          for(int i = 0; i < vehicleCount; i++){
            logger.info("inside the driverThree logic"+ i);
            //outVehicleAssignemt = new ArrayList<VehicleAssignment>();
            for(int j = 0; j < 5; j++){
            vehAssigment = new VehicleAssignment();
            driverAssignment = new DriverAssignment();
            vehAssigment.setId(i + 1);
            driverAssignment.setId(j + 1);
            driverAssignment.setContent(myArray[i][j]);
            vehAssigment.setDriverAssignment(driverAssignment);
            outVehicleAssignemt.add(vehAssigment);
           
            }
        }
         imAutoData.getVehicleAssignments().setVehicleAssignment(outVehicleAssignemt);
    	 logger.info("size of veh assignment is *********"+ outVehicleAssignemt.size());
    }
    private void driverSix(int vehicleCount){
          String[][] myArray = {{"18","17","17","16","16","16"}, {"17","18","17","16","16","16"}, {"17","17","18","16","16","16"},{"17","17","16","18","16","16"}};
           ArrayList<VehicleAssignment> outVehicleAssignemt = new ArrayList<VehicleAssignment>();
        VehicleAssignment vehAssigment;
            DriverAssignment driverAssignment;
          for(int i = 0; i < vehicleCount; i++){
            logger.info("inside the driverThree logic"+ i);
            //outVehicleAssignemt = new ArrayList<VehicleAssignment>();
            for(int j = 0; j < 6; j++){
            vehAssigment = new VehicleAssignment();
            driverAssignment = new DriverAssignment();
            vehAssigment.setId(i + 1);
            driverAssignment.setId(j + 1);
            driverAssignment.setContent(myArray[i][j]);
            vehAssigment.setDriverAssignment(driverAssignment);
            outVehicleAssignemt.add(vehAssigment);
           
            }
        }
         imAutoData.getVehicleAssignments().setVehicleAssignment(outVehicleAssignemt);
    	 logger.info("size of veh assignment is *********"+ outVehicleAssignemt.size());
    
    }
    
    private ArrayList<Carrier> restService(String carrierXML) {
        ArrayList<Carrier> carrierList = new ArrayList<Carrier>();
    	try {

    	    logger.info("*************Rating Service URL- "+ratingServiceUrl+"****************");
    		URL url = new URL(ratingServiceUrl);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setDoOutput(true);
    		conn.setRequestMethod("POST");
            conn.setRequestProperty ( "Content-Type", "text/xml" );
    		OutputStreamWriter out = new OutputStreamWriter(
conn.getOutputStream());
out.write(carrierXML);
out.flush();
out.close();
    		BufferedReader in = new BufferedReader(
		        new InputStreamReader(conn.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		 String soapmessageString = response.toString();
		          JSONObject soapDatainJsonObject = XML.toJSONObject(soapmessageString);
            System.out.println("JSONObject..." + soapDatainJsonObject);
            
            Iterator<String> keys = soapDatainJsonObject.keys();
JSONArray jarray = soapDatainJsonObject.getJSONObject("QuoteResponse").getJSONObject("PremiumData").getJSONObject("Carriers").getJSONArray("Carrier");
            System.out.println("JSONARRAY ..." + jarray);

            for(int i=0;i<jarray.length();i++){
                Carrier carrier = new Carrier();
                
                JSONObject carrierJson = jarray.getJSONObject(i);
                String comprater = carrierJson.getString("comprater");
                String applicantID = carrierJson.get("ApplicantID").toString();
                String id = carrierJson.get("ID").toString();
                String quoteExecutionID = carrierJson.has("QuoteExecutionID") ? carrierJson.get("QuoteExecutionID").toString() : null;
                String name = carrierJson.getString("Name");
                String policyNumber = carrierJson.getString("comprater");
                String insuraMatchID = soapDatainJsonObject.getJSONObject("QuoteResponse").get("InsuraMatchID").toString();
                String company = carrierJson.getString("comprater");
                
                carrier.setQuoteExecutionID(quoteExecutionID);
                carrier.setCompany(company);
                carrier.setName(name);
                carrier.setInsuraMatchID(insuraMatchID);
                carrier.setTotalPremium("");
                carrier.setId(id);
                
                carrierList.add(carrier);
    	}
    	conn.disconnect();
    	}
            

         catch (MalformedURLException e) {
    	    e.printStackTrace();} 
         catch (IOException e) {
                 e.printStackTrace();
         } 
         return carrierList;

    }



}
